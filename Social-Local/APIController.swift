//
//  APIController.swift
//  Social-Local
//
//  Created by Muhammad Nabil Hairul Fadilah on 24/03/2017.
//  Copyright © 2017 Epnox. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireImage

class APIController{
    
    //Variables
    var url = NSString()
    //var APIsession = user("","")
    
    func getRequestNoEncoding(purpose: String, completion: @escaping (Dictionary<String,AnyObject>) -> Void) {
        
        getURL(purpose : purpose)

        let nsurl : NSURL = NSURL(string: self.url as String)!
        let requestURL = URLRequest(url: nsurl as URL)
        
        Alamofire.request(requestURL)
            .responseJSON { response in
                
                guard response.result.isSuccess else {
                    
                    print("Error while fetching tags: \(String(describing: response.result.error))")
                    completion(Dictionary<String,AnyObject>())
                    return
                }
                
                guard let responseJSON = response.result.value as? Dictionary<String,AnyObject> else {
                    
                    print("Invalid tag information received from the service")
                    completion(Dictionary<String,AnyObject>())
                    return
                }
                
                completion(responseJSON)
        }
    }

    
    func getRequest(purpose: String, completion: @escaping (Dictionary<String,AnyObject>) -> Void) {
        
        getURL(purpose : purpose)
        
        let encodeURL : NSString = self.url.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed)! as NSString
        let nsurl : NSURL = NSURL(string: encodeURL as String)!
        let requestURL = URLRequest(url: nsurl as URL)
        //var responseValue = [String:AnyObject]()
        Alamofire.request(requestURL)
            .responseJSON { response in
                
                guard response.result.isSuccess else {
                    
                print("Error while fetching tags: \(String(describing: response.result.error))")
                    completion(Dictionary<String,AnyObject>())
                    return
                }
                
                guard let responseJSON = response.result.value as? Dictionary<String,AnyObject> else {
                    
                    print("Invalid tag information received from the service")
                    completion(Dictionary<String,AnyObject>())
                    return
                }
                
                completion(responseJSON)
                //responseValue = responseJSON
        }
    }
    
    func getImage(url:String, completion: @escaping(UIImage)-> Void){
        
        Alamofire.request(url).responseImage { response in
            if let image = response.result.value {
                completion(image)
            }
        }
        
    }

    func postRequest(purpose: String,parameter:Dictionary<String,AnyObject>, headers:Dictionary<String, String>,completion: @escaping (Dictionary<String,AnyObject>) -> Void) {
        
        getURL(purpose: purpose)
        
        let encodeURL : NSString = self.url.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed)! as NSString
        let requestURL = encodeURL as String
        print(requestURL)
        
        Alamofire.request(requestURL, method: .post, parameters: parameter, encoding: JSONEncoding.default, headers: headers) .responseJSON {
            response in
            switch response.result {
            case .success:
                
                if let result = response.result.value {
                    let JSON = result as! [String:AnyObject]
                    completion(JSON)
                    print(JSON)
                }
                
                break
                
            case .failure(let error):
                
                print(error)
            }
        }
        
        
        
    }
    
    private func convertToDictionary(text: String) -> [String: AnyObject]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String : AnyObject]
            } catch {
                print("Error converting string to dictionary: \(error.localizedDescription)")
            }
        }
        print("Invalid String, returning an empty dictionary[String: AnyObject]")
        return [String: AnyObject]()
    }

    
    private func getURL(purpose:String){
        
        let domain = "http://ec2-52-77-214-80.ap-southeast-1.compute.amazonaws.com/api/v1"
        let twilioDomain = "https://api.authy.com/protected/json/"
        print(twilio.api_key)
        
        switch purpose{

            case "twilioSignUp":
                self.url = "\(twilioDomain)users/new" as NSString
                print(self.url)
                break
            
            
            case "twilioStart":
                self.url = "\(twilioDomain)phones/verification/start" as NSString
                break
            
            case "twilioCheck":
                //Make a global "testNil" function for this
                var verification_code = String()
                if twilio.verification_code != nil{
                    verification_code = twilio.verification_code!
                    print(verification_code)
                }else{
                    verification_code = "null"
                }
                self.url = "\(twilioDomain)phones/verification/check?api_key=\(twilio.api_key)&country_code=\(twilio.country_code!)&phone_number=\(twilio.phone_number!)&verification_code=\(verification_code)" as NSString
                break
            
            case "user_resgistration":
                self.url = "\(domain)/user_sign_up" as NSString
                break
            
            //VALIDATE SESSION: USER
            case "validate_session_key":
                self.url = "\(domain)/validate_session_key/\(user.sessionKey ?? "null")" as NSString
                break
            
            //LOGIN : USER
            case "user_login":
                self.url = "\(domain)/user_login/\(user.email!)/\(user.password!)" as NSString
                break
            
            //LOGIN : SOCIAL NETWORK
            case "user_login_sn":
                self.url = "\(domain)/user_login_sn/\(user.email!)/\(userSession.accessKey!)/\(userSession.socialNetworkType!)" as NSString
                break
            
            //MAP: GET IMAGES
            case "get_images_by_gps":
                self.url = "\(domain)/get_images_by_gps/\(map.sessionKey!)/\(map.lat!)/\(map.long!)/\(map.radius!)/\(map.size!)" as NSString
            
            //GET : USER FOLLOWER
            case "get_user_follower":
                self.url = "\(domain)/get_user_follower/\(user.userID!)/\(user.sessionKey!)" as NSString
                break
            
            //GET : USER SOCIAL NETWORK KEY
            case "get_user_sn_key":
                self.url = "\(domain)/get_user_sn_key/\(user.userID!)/\(user.sessionKey!)" as NSString
                break
            
            //GET : USER CHAT KEY
            case "get_user_chat_key":
                self.url = "\(domain)/get_user_chat_key/\(user.userID!)/\(user.sessionKey!)" as NSString
                break
            
            //GET : USER PROFILE
            case "get_user_profile":
                print(user.sessionKey!, user.userID!)
                self.url = "\(domain)/get_user_profile/\(user.userID!)/\(user.sessionKey!)" as NSString
                break
            
            //POST : USER UPDATE PROFILE
            case "update_user_profile":
                self.url = "\(domain)/update_user_profile" as NSString
                break
            
            //GET : USER POSTING HISTORY
            case "get_post_history":
                self.url = "\(domain)/get_post_history/\(user.userID!)/\(user.sessionKey!)" as NSString
                print (user.userID!, user.sessionKey!)
                break
            
            //POST : INSERT LIKES
            case "insert_likes":
                self.url = "\(domain)/insert_likes" as NSString
                break
            
            //POST : REPORT IMAGE
            case "report_image":
                self.url = "\(domain)/report_image" as NSString
                break
        
            default:
                break
        }

    }
    
}
