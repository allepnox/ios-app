//
//  RegisterPicController.swift
//  Social-Local
//
//  Created by Epnox on 14/04/2017.
//  Copyright © 2017 Epnox. All rights reserved.
//

import UIKit

class RegisterPicController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    //VARIABLES
    @IBOutlet weak var userNameLabel:   UILabel!
    @IBOutlet weak var proPicImageView: UIImageView!
    
    //Recieve from RegPageController
    var email               = String()
    var password            = String()
    var phoneNumber         = String()
    var DOB                 = Double()
    var gender              = String()
    var country             = String()
    var areaCode            = String()
    var firebaseChatId      = String()
    var firebaseChatEntity  = String()
    var getImage: UIImage   = #imageLiteral(resourceName: "kid")
    
    
    //INSTANCES
    var alertController         = UIAlertController()
    let registrationController  = RegisterPageViewController()
    let networkController       = APIController()
    let loginController         = LoginViewController()
    
    /*----------------------------------------------------------START SWIFT FUNCTIONS-------------------------------------------------------------------------------------------*/
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        setupView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let TwilioSMS = segue.destination as! TwilioAPIController
        TwilioSMS.phoneNumber           = phoneNumber
        TwilioSMS.areaCode              = areaCode
        TwilioSMS.email                 = email
        TwilioSMS.password              = password
        TwilioSMS.DOB                   = DOB
        TwilioSMS.gender                = gender
        print(gender, email)
        TwilioSMS.country               = country
        TwilioSMS.firebaseChatId        = firebaseChatId
        TwilioSMS.firebaseChatEntity    = firebaseChatEntity
        TwilioSMS.getImage              = getImage
        print(getImage, TwilioSMS.DOB, DOB)
        
    }
    
    /*----------------------------------------------------------END SWIFT FUNCTIONS---------------------------------------------------------------------------------------------*/
    
    
    /*----------------------------------------------------------START BUTTON FUNCTIONS------------------------------------------------------------------------------------------*/
    
    //SET PIC BUTTON
    @IBAction func setPicButtonTapped(_ sender: UIButton) {
       self.present(alertController, animated: true, completion: nil)
    }
    //COMPLETE BUTTON
    @IBAction func completeButtonTapped(_ sender: UIButton) {
    
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: "regPicToSMS", sender: self)
        }
    }
    
    /*----------------------------------------------------------END BUTTON FUNCTIONS--------------------------------------------------------------------------------------------*/
    
    
    /*-----------------------------------------------------------START PAGE FUNCTIONS-------------------------------------------------------------------------------------------*/
    
    
    //SETUP VIEW
    func setupView(){
        actionSheetView()
        proPicImageView?.layer.cornerRadius = (proPicImageView?.frame.size.width)! / 2
        proPicImageView?.clipsToBounds = true
    }
    
    //ACTION SHEET FOR CAMERA AND GALLARY
    func actionSheetView()
    {
        alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let cameraAction = UIAlertAction(title: "Camera", style: .default, handler: { action in
            self.openCamera()
        })
        let libraryAction = UIAlertAction(title: "Photo Library", style: .default, handler: { action in
            self.openLibrary()
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
        
        alertController.addAction(cameraAction)
        alertController.addAction(libraryAction)
        alertController.addAction(cancelAction)
    }
    
    //HANDLE IMAGE FROM CAMERA/GALLARY
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        self.getImage = (info[UIImagePickerControllerEditedImage] as? UIImage)!
        proPicImageView.image = self.getImage
        
        //uploadProfileImage(image: getImage!)
        
        self.dismiss(animated: true, completion: nil)
    }
    
    
    //OPEN CAMERA
    func openCamera()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)
        {
            let imageCamera = UIImagePickerController()
            imageCamera.delegate = self
            imageCamera.sourceType = UIImagePickerControllerSourceType.camera
            imageCamera.allowsEditing = true
            self.present(imageCamera, animated: true, completion: nil)
            
        }else{
            print("cannot open camera")
        }
    }
    
    // OPEN LIBRARY
    func openLibrary()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary)
        {
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self
            myPickerController.sourceType = UIImagePickerControllerSourceType.photoLibrary
            myPickerController.allowsEditing = true
            
            self.present(myPickerController, animated: true, completion: nil)
        }else
        {
            //should probably put a alert here
            print ("cannot open library")
        }
    }
    /*---------------------------------------------------------- END PAGE FUNCTIONS ------------------------------------------------------------------------------------------*/
}
