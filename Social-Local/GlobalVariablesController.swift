//
//  GlobalVariablesViewController.swift
//  Social-Local
//
//  Created by Epnox on 24/03/2017.
//  Copyright © 2017 Epnox. All rights reserved.
//

import UIKit

struct twilio{
    
    static var api_key             = "NlngcmlwnNvwuZfGhfUVSO6ulRBhWS05"
    static var via                 = "sms"
    static var atRegisterFlag      = false
    
    static var locale               : String?
    static var phone_number         : String?
    static var country_code         : String?
    static var verification_code    : String?
    static var email                : String?
    
    //START INIT
    init(verification_code:String) {
        twilio.verification_code  = verification_code
    }
    
    //CHECK INIT
    init(phone_number:String, country_code:String, locale:String, email: String){
        twilio.phone_number       = phone_number
        twilio.country_code       = country_code
        twilio.locale             = locale
        twilio.email              = email
        
    }
    
    
}

struct userSession{
    
    static var accessKey        : String?
    static var socialNetworkType: String?
    static var firebaseKey      : String?
    
    init(accessKey:String,socialNetworkType:String,firebaseKey:String){
        userSession.accessKey           = accessKey
        userSession.socialNetworkType   = socialNetworkType
        userSession.firebaseKey         = firebaseKey
    }
}

struct user{
    
    static var sessionKey   : String? //local social
    static var userID       : String? //
    static var email        : String?
    static var password     : String?
    
    init(key:String?, id:String?){
        
        user.sessionKey = key
        user.userID     = id
    }
    
    init(email: String?, password: String?){
        user.email = email
        
        if password == nil{
            user.password = " "
        }else{
            user.password = password
        }
    }

}
struct firebase {
    
    static var fire_Session_Key: String?
    
    init(fire_Session_Key: String) {
        firebase.fire_Session_Key = fire_Session_Key
    }
}

struct profile{
    
    static var remaining_coin   : String?
    static var phone            : String?
    static var follower_count   : Int?
    static var image_src        : String?
    static var gender           : String?
    static var date_of_birth    : Double?
    static var post_count       : Int?
    static var username         : String?
    static var hometown         : String?
    
    init(remaining_coin:String,phone:String,follower_count:Int,image_src:String,gender:String,date_of_birth:Double,post_count:Int,username:String,hometown:String){
        profile.remaining_coin  = remaining_coin
        profile.phone           = phone
        profile.follower_count  = follower_count
        profile.image_src       = image_src
        profile.gender          = gender
        profile.date_of_birth   = date_of_birth
        profile.post_count      = post_count
        profile.username        = username
        profile.hometown        = hometown
    }
}

struct map{
    
    static var lat          : Double?
    static var long         : Double?
    static var radius       : Double?
    static var sessionKey   : String?
    static var size         : String?
    
    init(lat: Double, long: Double, radius: Double, sessionKey: String, size: String){
        
        map.lat         = lat
        map.long        = long
        map.radius      = radius
        map.sessionKey  = sessionKey
        map.size        = size
    }
}
