//
//  PromptController.swift
//  Social-Local
//
//  Created by Epnox on 04/05/2017.
//  Copyright © 2017 Epnox. All rights reserved.
//

import UIKit
//A class to prompt the user with messages
class GlobalFuncsController: UIViewController {


    func displayAlertMessage(title: String, buttonTittle: String, message: String, action: ())-> UIAlertController{
        let theAlert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        let okAction = UIAlertAction(title: buttonTittle, style: UIAlertActionStyle.default, handler: {(alert: UIAlertAction!) in action})
        theAlert.addAction(okAction)
        
        return theAlert
    }
    
    //DEFAULT function if you want nothing done when "OK" is tapped
    func defaultAction(){
        print("This works, say wah!")
    }

}
