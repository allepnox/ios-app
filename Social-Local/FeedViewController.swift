//
//  FeedViewController.swift
//  Social-Local
//
//  Created by Muhammad Nabil Hairul Fadilah on 22/03/2017.
//  Copyright © 2017 Epnox. All rights reserved.
//


import UIKit

class FeedViewController: UIViewController {
    
    @IBAction func logoutButtonTapped(_ sender: UIButton) {
        //clear all user defaults
        UserDefaults.standard.removeObject(forKey: "sessionKey")
        UserDefaults.standard.removeObject(forKey: "user_id")
        UserDefaults.standard.removeObject(forKey: "email")
        UserDefaults.standard.synchronize()
        
        //send user back to login page
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: "tempLogout", sender: self)
        }
    }
    
}
