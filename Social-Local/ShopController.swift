//
//  Shop.swift
//  Social-Local
//
//  Created by Muhammad Nabil Hairul Fadilah on 24/04/2017.
//  Copyright © 2017 Epnox. All rights reserved.
//

import UIKit

class ShopController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var CoinBalance: UILabel!
    @IBOutlet weak var TableShop: UITableView!
    
    var result = [AnyObject]()
    var coin : String?
    var numberOfRow = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        APIController().getRequest(purpose: ""){ data in
            
            if data.isEmpty{
                print("data is empty")
            }else{
                self.result = data["results"] as! [AnyObject]
                self.numberOfRow = (data["results"]?.row)!
                self.TableShop.reloadData()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.numberOfRow
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = TableShop.dequeueReusableCell(withIdentifier: "coinCell", for: indexPath) as! ProfileCell
        
        let value = self.result[indexPath.row]
        cell.numberCoin.text = value["hour"] as? String
        cell.priceCoin.setTitle(value["coins"] as? String, for: UIControlState())
        
        return cell
    }
    
    @IBAction func BackButton(_ sender: Any) {
        self.dismiss(animated: true , completion: nil)
    }

}
