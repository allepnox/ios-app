//
//  CameraViewController.swift
//  Social-Local
//
//  Created by Muhammad Nabil Hairul Fadilah on 22/03/2017.
//  Copyright © 2017 Epnox. All rights reserved.
//

import UIKit
import CameraManager

class CameraViewController: UIViewController , UIImagePickerControllerDelegate, UINavigationControllerDelegate
{
    // MARK: - Constants
    let cameraManager = CameraManager()
    
    // MARK: - @IBOutlets
    
    @IBOutlet weak var cameraView: UIView!
    
    @IBOutlet weak var cameraButtonInner: UIButton!
    @IBOutlet weak var cameraButton: UIButton!
    @IBOutlet weak var flashModeButton: UIButton!
    
    @IBOutlet weak var permissionLabel: UILabel!
    @IBOutlet weak var permissionButton: UIButton!
    @IBOutlet weak var imageHolder: UIImageView!
    
    var takenPhoto = UIImage()
    
    
    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        cameraButton.layer.cornerRadius = cameraButton.frame.size.width / 2
        cameraButton.clipsToBounds = true
        cameraButtonInner.layer.cornerRadius = cameraButtonInner.frame.size.width / 2
        cameraButton.clipsToBounds = true
        
        cameraManager.showAccessPermissionPopupAutomatically = false
        
        permissionButton.isHidden = true
        permissionLabel.isHidden = true
        
        let currentCameraState = cameraManager.currentCameraStatus()
        
        if currentCameraState == .notDetermined {
            permissionButton.isHidden = false
            permissionLabel.isHidden = false
        } else if (currentCameraState == .ready) {
            addCameraToView()
        }
        if !cameraManager.hasFlash {
            flashModeButton.isEnabled = false
            flashModeButton.setTitle("No flash", for: UIControlState())
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.navigationBar.isHidden = true
        cameraManager.resumeCaptureSession()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        cameraManager.stopCaptureSession()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "postEditingSegue"
        {
            if let postDetailVC : PostingController = segue.destination as? PostingController
            {
                postDetailVC.takenPhoto = self.takenPhoto
            }
        }else{
            return
        }
    }
    
    
    // MARK: - ViewController
    
    fileprivate func addCameraToView()
    {
        _ = cameraManager.addPreviewLayerToView(cameraView, newCameraOutputMode: CameraOutputMode.stillImage)
        cameraManager.showErrorBlock = { [weak self] (erTitle: String, erMessage: String) -> Void in
            
            let alertController = UIAlertController(title: erTitle, message: erMessage, preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (alertAction) -> Void in  }))
            
            self?.present(alertController, animated: true, completion: nil)
        }
    }
    
    //handler image that been selected in library or been capture
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        let getImage =  info[UIImagePickerControllerOriginalImage] as! UIImage
        self.takenPhoto = getImage
        self.imageHolder.image = getImage
        self.dismiss(animated: true, completion: {
            self.performSegue(withIdentifier: "postEditingSegue", sender: nil)
        })
    }
    
    // MARK: - @IBActions
    
    @IBAction func changeFlashMode(_ sender: UIButton)
    {
        switch (cameraManager.changeFlashMode()) {
        case .off:
            sender.setTitle("Flash Off", for: UIControlState())
        case .on:
            sender.setTitle("Flash On", for: UIControlState())
        case .auto:
            sender.setTitle("Flash Auto", for: UIControlState())
        }
    }
    
    @IBAction func recordButtonTapped(_ sender: UIButton) {
        
        switch (cameraManager.cameraOutputMode) {
        case .stillImage:
            cameraManager.capturePictureWithCompletion({ (image, error) -> Void in
                if let errorOccured = error {
                    self.cameraManager.showErrorBlock("Error occurred", errorOccured.localizedDescription)
                }
                else {
                    self.takenPhoto = image!
                    self.performSegue(withIdentifier: "postEditingSegue", sender: nil)
                }
            })
        case .videoWithMic, .videoOnly:
            sender.isSelected = !sender.isSelected
            sender.setTitle(" ", for: UIControlState.selected)
            sender.backgroundColor = sender.isSelected ? UIColor.red : UIColor.green
            if sender.isSelected {
                cameraManager.startRecordingVideo()
            } else {
                cameraManager.stopVideoRecording({ (videoURL, error) -> Void in
                    if let errorOccured = error {
                        self.cameraManager.showErrorBlock("Error occurred", errorOccured.localizedDescription)
                    }
                })
            }
        }
    }
    
    @IBAction func outputModeButtonTapped(_ sender: UIButton) {
        
        cameraManager.cameraOutputMode = cameraManager.cameraOutputMode == CameraOutputMode.videoWithMic ? CameraOutputMode.stillImage : CameraOutputMode.videoWithMic
        switch (cameraManager.cameraOutputMode) {
        case .stillImage:
            cameraButton.isSelected = false
            cameraButton.backgroundColor = UIColor.green
            sender.setTitle("Image", for: UIControlState())
        case .videoWithMic, .videoOnly:
            sender.setTitle("Video", for: UIControlState())
        }
    }
    
    @IBAction func changeCameraDevice(_ sender: UIButton) {
        
        cameraManager.cameraDevice = cameraManager.cameraDevice == CameraDevice.front ? CameraDevice.back : CameraDevice.front
        switch (cameraManager.cameraDevice) {
        case .front:
            sender.setTitle("Front", for: UIControlState())
        case .back:
            sender.setTitle("Back", for: UIControlState())
        }
    }
    
    @IBAction func askForCameraPermissions(_ sender: UIButton) {
        
        cameraManager.askUserForCameraPermission({ permissionGranted in
            self.permissionButton.isHidden = true
            self.permissionLabel.isHidden = true
            self.permissionButton.alpha = 0
            self.permissionLabel.alpha = 0
            if permissionGranted {
                self.addCameraToView()
            }
        })
    }
    @IBAction func OpenGallery(_ sender: Any) {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary)
        {
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self
            myPickerController.sourceType = UIImagePickerControllerSourceType.photoLibrary
            myPickerController.allowsEditing = false
            
            self.present(myPickerController, animated: true, completion: nil)
        }else
        {
            print ("cannot open library")
        }
    }
    
}
