//
//  AnimationsViewController.swift
//  Social-Local
//
//  Created by Epnox on 13/04/2017.
//  Copyright © 2017 Epnox. All rights reserved.
//

import UIKit

class AnimationsViewController: UIViewController {
    //VARIABLES
    
    //INSTANCES
    var activityIndicator = UIActivityIndicatorView()
    
    
    func indicatorStart()->UIActivityIndicatorView{
        self.activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        self.activityIndicator.center = view.center
        self.activityIndicator.hidesWhenStopped = true
        self.activityIndicator.startAnimating()
        UIApplication.shared.beginIgnoringInteractionEvents()
        return activityIndicator
    }
    
    func indicatorStop(){
        activityIndicator.stopAnimating()
        UIApplication.shared.endIgnoringInteractionEvents()
    }
}
