//
//  MasterProfileController.swift
//  Social-Local
//
//  Created by Muhammad Nabil Hairul Fadilah on 28/03/2017.
//  Copyright © 2017 Epnox. All rights reserved.
//

import UIKit

class MasterProfileController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    
    // Table View Object
    @IBOutlet weak var segmentProfile: UISegmentedControl!
    @IBOutlet weak var ChatChildController: UIView!
    
    // Profile View Object
    @IBOutlet weak var profilePicture: UIImageView!
    @IBOutlet weak var followerCount: UILabel!
    @IBOutlet weak var postCount: UILabel!
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var editProfile: UIButton!
    @IBOutlet weak var postCollectionView: UICollectionView!
    
    var dateFormatter = DateFormatter()
    var alertController = UIAlertController()
    var count = Int()
    var result = [AnyObject]()
    var holdData = [String:AnyObject]()
    var PostImages = [AnyObject]()
    var PostCache : Bool = false
    let Cache = CacheController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.username.text = profile.username
        self.postCount.text = "\(profile.post_count!)"
        self.followerCount.text = "\(profile.follower_count!)"
        uploadProfileImage() //uploading image of profile picture
        
        APIController().getRequest(purpose:"get_post_history")
        { data in
            
            self.result = data["results"] as! [AnyObject]
            self.count = data["post_count"] as! Int
            self.postCollectionView.reloadData()
        }
        
        setupView()
    }
    
    private func setupView(){
        setupSegmentControl()
        actionSheetView()
    }
    
    func uploadProfileImage(){
        if !(profile.image_src?.isEmpty)!{
            APIController().getImage(url: profile.image_src!){image in
                self.profilePicture.image = image
            }
        }
    }
    
    //MARK - SEGMENT CONTROL
    private func setupSegmentControl(){
        
        segmentProfile.removeAllSegments()
        segmentProfile.insertSegment(withTitle: "Chat", at: 0, animated: false)
        segmentProfile.insertSegment(withTitle: "Posts", at: 1, animated: false)
        
        segmentProfile.selectedSegmentIndex = 0
        
        editProfile.backgroundColor = .clear
        editProfile.layer.cornerRadius = 5
        editProfile.layer.borderWidth = 1
        editProfile.layer.borderColor = UIColor.darkGray.cgColor
        
        self.profilePicture.layer.cornerRadius = self.profilePicture.frame.size.width / 2
        self.profilePicture.clipsToBounds = true
    }
    
    //MARK - Setting Option
    func actionSheetView()
    {
        alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let profileAction = UIAlertAction(title: "Profile", style: .default, handler: { action in
            DispatchQueue.main.async {
                self.performSegue(withIdentifier: "settingSegue", sender: self)
            }
        })
        let privacyAction = UIAlertAction(title: "Privacy", style: .default)
        let chatAction = UIAlertAction(title: "Chat", style: .default)
        let logoutAction = UIAlertAction(title: "Logout", style: .default, handler:{ action in
            DispatchQueue.main.async {
                self.Cache.clearCache()
                self.performSegue(withIdentifier: "logoutSegue", sender: self)
            }
        })
        logoutAction.setValue(UIColor.red, forKey: "titleTextColor")
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
        
        alertController.addAction(profileAction)
        alertController.addAction(privacyAction)
        alertController.addAction(chatAction)
        alertController.addAction(logoutAction)
        alertController.addAction(cancelAction)
    }
    
    //MARK - Post Collection View
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return profile.post_count!
    }
     
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionCell", for: indexPath) as! CollectionCell
        if self.count == profile.post_count {
            let item = result[indexPath.row]
            DispatchQueue.main.async(execute: {
                
                let urlImage = item["image_src"] as! String
                cell.CollectionImage.sd_setImage(with: URL(string: urlImage))
            })
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let value = self.result[indexPath.row]
        self.holdData = [
            "image_id"      : value["image_id"] as AnyObject,
            "image_src"     : value["image_src"] as AnyObject,
            "timestamp"     : value["created_on"] as AnyObject,
            "location"      : value["location"] as AnyObject,
            "caption"       : value["caption"] as AnyObject,
            "likes"         : value["likes"] as AnyObject
            
        ]
        self.performSegue(withIdentifier: "postDetailsSegue", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "postDetailsSegue"{
            if let postDetailVC : PostDetailsController = segue.destination as? PostDetailsController
            {
                postDetailVC.data = self.holdData
            }
        }else if segue.identifier == "logoutSegue"{
            
            if let loginVC : LoginViewController = segue.destination as? LoginViewController
            {
                loginVC.emailGlobal = user.email!
            }
        }else{
            return
        }
    }
    
    //MARK - Segment Button Selected (Press)
    
    @IBAction func segmentController2(_ sender: UISegmentedControl) {
        
        switch sender.selectedSegmentIndex{
        case 0:
            UIView.animate(withDuration: 0.5, animations: {
                self.ChatChildController.alpha = 1.0
                self.postCollectionView.alpha = 0.0
            })
            
        case 1:
            UIView.animate(withDuration: 0.5, animations: {
                self.ChatChildController.alpha = 0.0
                self.postCollectionView.alpha = 1.0
            })
            
        default:
            UIView.animate(withDuration: 0.5, animations: {
                self.ChatChildController.alpha = 1.0
                self.postCollectionView.alpha = 0.0
            })
        }
    }
    
    @IBAction func FollowerButton(_ sender: Any) {
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: "followerSegue", sender: self)
        }
        
    }
    
    @IBAction func postButton(_ sender: Any) {
        segmentProfile.selectedSegmentIndex = 1
        UIView.animate(withDuration: 0.5, animations: {
            self.ChatChildController.alpha = 0.0
            self.postCollectionView.alpha = 1.0
        })
    }
    @IBAction func settingButton(_ sender: Any) {
        self.present(alertController,animated: true, completion:nil)
    }
    @IBAction func editProfileButton(_ sender: Any) {
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: "settingSegue", sender: self)
        }
    }
    
}
