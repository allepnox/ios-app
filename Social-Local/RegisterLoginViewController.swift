//
//  RegisterLoginViewController.swift
//  
//
//  Created by Epnox on 07/05/2017.
//
//

import UIKit

class RegisterLoginViewController: UIViewController {
    
    let NetworkController       = APIController()
    let GlobalFuncController    = GlobalFuncsController()
    let LoginController         = LoginViewController()
    var registerFlag: Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("Loaded")

    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.NetworkController.getRequest(purpose: "user_login"){
            data in
            self.loggInToSocialLocal(data: data)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
  
    private func loggInToSocialLocal(data: [String: AnyObject]){
        print(data)
        registerFlag = true
        let loginFlag = LoginController.loginToAPP(data: data, email: user.email!, password: user.password!)
        
        if (loginFlag == true){
            DispatchQueue.main.async {
                self.performSegue(withIdentifier: "regToNav", sender: self)
            }
        }else{
            print("Error loging in")
            let _ = GlobalFuncController.displayAlertMessage(title: "Error", buttonTittle: "OK", message: "Could not register!", action: ())
            //maybe segue back to the registerView 1st one.
        }
    }


}
