//
//  LocationController.swift
//  Social-Local
//
//  Created by Epnox on 14/04/2017.
//  Copyright © 2017 Epnox. All rights reserved.
//

import UIKit
import GooglePlaces

class LocationController: UIViewController, CLLocationManagerDelegate {
    //INSTANCES
    let locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
            var _:CLLocation = locations[0]
            locationManager.stopUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            locationManager.startUpdatingLocation()
        }
    }
}
