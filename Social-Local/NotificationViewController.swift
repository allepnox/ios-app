//
//  NotificationViewController.swift
//  Social-Local
//
//  Created by Muhammad Nabil Hairul Fadilah on 22/03/2017.
//  Copyright © 2017 Epnox. All rights reserved.
//

import UIKit

class NotificationViewController: UIViewController,UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableNotification: UITableView!
    var results = [AnyObject]()
    var count = 0
    let dateFormatter = DateFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        APIController().getRequest(purpose: "get_notification") { data in
            print (data)
            if data.isEmpty{
                print ("data is empty")
            }else{
                self.results = data["results"] as! [AnyObject]
                self.count = data["count"] as! Int
                
                self.tableNotification.reloadData()
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableNotification.dequeueReusableCell(withIdentifier: "notificationCell", for: indexPath) as! ProfileCell
        
        cell.imageNotification.layer.cornerRadius = cell.imageNotification.frame.size.width / 2
        cell.imageNotification.clipsToBounds = true
        
        if let item : [String:AnyObject] = self.results[indexPath.row] as? [String : AnyObject]{
            DispatchQueue.main.async(execute: {
                if !(item["profile_img_src"] is NSNull){
                    let urlImage = item["profile_img_src"] as! String
                    APIController().getImage(url: urlImage){ image in
                        cell.imageNotification.image = image
                    }
                }
            })
            dateFormatter.dateFormat = "dd-MM-YYYY hh-mm-ss"
            cell.titleNotification.text = item["title"] as! String?
            let notiDate = DateController().unixToDate(item["create_datetime"] as! Double)
            cell.timestampNotification.text = dateFormatter.string(from: notiDate)
            
            cell.messageNotification.text = item["message"] as! String?
        }
        
        
        return cell
    }
}
