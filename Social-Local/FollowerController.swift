//
//  Second_Container.swift
//  Social-Local
//
//  Created by Muhammad Nabil Hairul Fadilah on 28/03/2017.
//  Copyright © 2017 Epnox. All rights reserved.
//

import UIKit
import Alamofire

class FollowerController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableFollower: UITableView!
    var arrayList = [AnyObject]()
    var count = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        APIController().getRequest(purpose: "get_user_follower"){data in
            self.arrayList = data["results"] as! [AnyObject]
            self.count = data["follower_count"] as! Int;()
            self.tableFollower.reloadData()
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableFollower.dequeueReusableCell(withIdentifier: "followerCell", for: indexPath) as! ProfileCell
        
        cell.photoFollower.layer.cornerRadius = cell.photoFollower.frame.size.width / 2
        cell.photoFollower.clipsToBounds = true
        let item = self.arrayList[indexPath.row]
        DispatchQueue.main.async(execute: {
            if !(item["image_src"] is NSNull){
                let url_image = item["image_src"] as! String
                print (url_image)
                APIController().getImage(url: url_image){ data in
                    cell.photoFollower.image = data
                }
            }
        })
        cell.nameFollower.text = item["username"] as! String?
        
        
 
        return cell
    }
    
    @IBAction func ReturnBack(_ sender: Any) {
        if let nav = self.navigationController {
            nav.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }

}
