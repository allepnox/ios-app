//
//  functionController.swift
//  Social-Local
//
//  Created by Muhammad Nabil Hairul Fadilah on 17/04/2017.
//  Copyright © 2017 Epnox. All rights reserved.
//

import UIKit

class DateController {
    
    let dateFormatter = DateFormatter()
    
    func dateToUnix(_ date:Date) -> Double{
        let unix = date.timeIntervalSince1970 * 1000
        print (unix)
        return unix
    }
    
    func unixToDate(_ unix:Double) -> Date {
        
        let timeInterval = TimeInterval(unix) / 1000
        let date = Date(timeIntervalSince1970: timeInterval)
        print (date)
        return date
    }
    
    func dateToString(_ date:Date) -> String{
        
        dateFormatter.dateFormat = "dd-MM-YYYY"
        let dateinString = dateFormatter.string(from: date)
        print (dateinString)
        return dateinString
    }
}
