//
//  RegisterPageViewController.swift
//  Social-Local
//
//  Created by Remigius Kalimba on 21/03/2017.
//  Copyright © 2017 Epnox. All rights reserved.
//

import UIKit
import GooglePlaces
import CoreTelephony

class RegisterPageViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    let placesAPI = "AIzaSyCDxqYilwlOi-nWBav8LeWZiiRrObriKAc"
    
    func test(){
        userEmailTextField.text = "remi@k.com"
        userPassTextField.text = "12345678"
        retypePassTextField.text = "12345678"
        phoneNumTextField.text = "149277532"
        genderTextField.text = "Male"
    }
    
    //VARIABLES FROM STORY
    @IBOutlet weak var userEmailTextField: UITextField!
    @IBOutlet weak var userPassTextField: UITextField!
    @IBOutlet weak var retypePassTextField: UITextField!
    @IBOutlet weak var phoneNumTextField: UITextField!
    @IBOutlet weak var dobTextField: UITextField!
    @IBOutlet weak var genderTextField: UITextField!
    @IBOutlet weak var countryTextField: UITextField!
    @IBOutlet weak var areaCodeTextField: UITextField!
    
    let dateFormatter   = DateFormatter()
    let datePicker      = UIDatePicker()
    let picker          = UIPickerView()
    let toolbar_dob     = UIToolbar()
    let toolbar_gender  = UIToolbar()
    var alertController = UIAlertController()
    
    //INIT VARIABLES
    let genderArray     = ["Male","Female","Others"]
    var selectedGender  = String()
    var selectedDOB     = Date()

    
    //INSTANCES
    let networkController   = APIController()
    let locationManager     = CLLocationManager()
    let phoney              = PhoneController.self
    let DateControler       = DateController()
    let GlobalFunc         = GlobalFuncsController()
    
    /*----------------------------------------------------------START SWIFT FUNCTIONS-------------------------------------------------------------------------------------------*/
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //INIT LOCATION MANAGER
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
        
        
        //Looks for single or multiple taps.
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(RegisterPageViewController.dismissKeyboard))
        self.view.addGestureRecognizer(tap)
        
        setupView()
        toolbar()
        get_location()
        test()
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        

    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let RegPicController = segue.destination as! RegisterPicController
        //set variable in the registerPicController
        RegPicController.email          =   userEmailTextField.text!
        RegPicController.password       =   userPassTextField.text!
        RegPicController.phoneNumber    =   phoneNumTextField.text!
        RegPicController.DOB            =   DateControler.dateToUnix(self.selectedDOB)
        RegPicController.gender         =   self.selectedGender
        RegPicController.country        =   countryTextField.text!
        RegPicController.areaCode       =   areaCodeTextField.text!
        
    }
    
    
    /*----------------------------------------------------------END SWIFT FUNCTIONS-------------------------------------------------------------------------------------------*/

    
    func genderSelection(gender:String)
    {
        if gender == "1" || gender == "M" || gender == "Male" {
            self.genderTextField.text = "Male"
            self.selectedGender = "M"
            picker.selectRow(0, inComponent: 0, animated: true)
        }else if gender == "2" || gender == "F" || gender == "Female"{
            self.genderTextField.text = "Female"
            self.selectedGender = "F"
            picker.selectRow(1, inComponent: 0, animated: true)
        }else{
            self.genderTextField.text = "Others"
            self.selectedGender = "O"
            picker.selectRow(2, inComponent: 0, animated: true)
        }
        
    }

    
    //DISMISSKEYBOARD
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }

    
    func setupView(){
        dateFormatter.dateFormat = "dd/MM/yyyy"
    }  
    
    
    //function for date of birth and gender to select the value
    func toolbar(){
        datePicker.datePickerMode = .date
        
        picker.delegate = self
        picker.dataSource = self
        
        toolbar_dob.sizeToFit()
        toolbar_gender.sizeToFit()
        
        let doneButton_dob = UIBarButtonItem(barButtonSystemItem: .done, target : nil, action: #selector(buttonDone_dob))
        let cancelButton_dob = UIBarButtonItem(barButtonSystemItem: .cancel, target : nil, action: #selector(buttonClose))
        let cancelButton_gender = UIBarButtonItem(barButtonSystemItem: .cancel, target : nil, action: #selector(buttonClose))
        let doneButton_gender = UIBarButtonItem(barButtonSystemItem: .done, target : nil, action: #selector(buttonDone_gender))
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        
        cancelButton_dob.tintColor = UIColor.red
        cancelButton_gender.tintColor = UIColor.red
        
        toolbar_dob.setItems([doneButton_dob,flexibleSpace,cancelButton_dob], animated: true)
        toolbar_gender.setItems([doneButton_gender,flexibleSpace,cancelButton_gender], animated: true)
        
        dobTextField.inputAccessoryView =  toolbar_dob
        dobTextField.inputView = datePicker
        genderTextField.inputAccessoryView = toolbar_gender
        genderTextField.inputView = picker
        
        //xcode remove adatePicker.date = DateController().unixToDate(Calendar.current)
        
    }
    
    
    //button done for date of birth at toolbar
    func buttonDone_dob()
    {
        print (datePicker.date)
        let dob = dateFormatter.string(from: datePicker.date)
        print (dob)
        self.dobTextField.text = dob
        self.selectedDOB = Calendar.current.date(byAdding: .day, value: 1, to: datePicker.date)! 
        print (selectedDOB)
        self.view.endEditing(true)
    }
    
    
    //button done for gender at toolbar
    func buttonDone_gender(){
        self.genderTextField.text = self.selectedGender
        self.view.endEditing(true)
    }
    
    
    //button close at toolbar
    func buttonClose()
    {
        self.view.endEditing(true)
    }
    
    
    //this is for gender picker function : number of components
    func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
        return 1
    }
    
    
    //this is for gender picker function : number of rows
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        return genderArray.count
    }
    
    
    //this is for gender picker function : title for row
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        return genderArray[row]
    }
    
    //this is for gender picker function : selecting row when appear
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        self.selectedGender = self.genderArray[row]
        
    }
    
    //EMAIL VALIDATION
    private func isValidEmail(testEmail:String) -> Bool {
        let emailRegEx = "^(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?(?:(?:(?:[-A-Za-z0-9!#$%&’*+/=?^_'{|}~]+(?:\\.[-A-Za-z0-9!#$%&’*+/=?^_'{|}~]+)*)|(?:\"(?:(?:(?:(?: )*(?:(?:[!#-Z^-~]|\\[|\\])|(?:\\\\(?:\\t|[ -~]))))+(?: )*)|(?: )+)\"))(?:@)(?:(?:(?:[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)(?:\\.[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)*)|(?:\\[(?:(?:(?:(?:(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))\\.){3}(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))))|(?:(?:(?: )*[!-Z^-~])*(?: )*)|(?:[Vv][0-9A-Fa-f]+\\.[-A-Za-z0-9._~!$&'()*+,;=:]+))\\])))(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?$"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: testEmail)
        return result
    }
    
    private func formatPhoneNumber(number: String)-> String{
        let num = NSMutableString(string: number)
        let indexOfZero = number.index(number.startIndex, offsetBy: 3)
        let indexOfFive = number.index(number.startIndex, offsetBy: 7)
        
        //num.insert("(", at: 0)
        //num.insert(")", at: 4)
        if number[indexOfZero] != "-" {
            num.insert("-", at: 3)
        }
        
        if number[indexOfFive] != "-" {
            num.insert("-", at: 7)
        }
        
        
        return num as String
    }
    
    //PHONENUMBER VALIDATION
    private func isValidPhoneNumber(number: String) -> Bool {
        let PHONE_REGEX = "^[0-9]{3}-[0-9]{3}-[0-9]{3,}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: number)
        return result
    }
    
    //CHECK ALL ENTRIES
    private func checkEntries(){
        let userEmail = userEmailTextField.text
        let userPassword = userPassTextField.text
        let userRetypePass = retypePassTextField.text
        let phoneNum = phoneNumTextField.text
        let DOB = dobTextField.text
        let Gender = genderTextField.text
        
        //check for empty fields
        if((userEmail?.isEmpty)! || (userPassword?.isEmpty)! || (userRetypePass?.isEmpty)! || (phoneNum?.isEmpty)! || (DOB?.isEmpty)! || (Gender?.isEmpty)!){
            let theAlert = GlobalFunc.displayAlertMessage(title: "Alert", buttonTittle: "OK", message: "All fields are required!", action: ())
            self.present(theAlert, animated: true, completion: nil)
            return
        }
        
        //CHECK EMAIL
        if isValidEmail(testEmail: userEmail!) != true{
            let theAlert = GlobalFunc.displayAlertMessage(title: "Alert", buttonTittle: "OK", message: "That is not a valid email: example@mail.com", action: GlobalFunc.defaultAction())
            self.present(theAlert, animated: true, completion: nil)
            return
        }
        
        //CHECK PASSWORDS MATCH and length
        if(userPassword?.characters.count)! < 8{
            let theAlert = GlobalFunc.displayAlertMessage(title: "Alert", buttonTittle: "OK", message: "Password should be atleast 8 characters", action: GlobalFunc.defaultAction())
            self.present(theAlert, animated: true, completion: nil)
            return
        }
        if(userPassword != userRetypePass){
            let theAlert = GlobalFunc.displayAlertMessage(title: "Alert", buttonTittle: "OK", message: "Passwords do not match", action: GlobalFunc.defaultAction())
            self.present(theAlert, animated: true, completion: nil)
            return
        }

        //CHECK PHONENUMBER
        let num = formatPhoneNumber(number: phoneNum!)
        let result = isValidPhoneNumber(number: num)
        if result != true{
            //Set placeholder text as old number
            phoneNumTextField.placeholder = num as String
            phoneNumTextField.text = ""
            //prompt user
            let theAlert = GlobalFunc.displayAlertMessage(title: "Alert", buttonTittle: "OK", message: "phone number must be 9 digits, excluding country code", action: GlobalFunc.defaultAction())
            self.present(theAlert, animated: true, completion: nil)
            return
        }
        
        //FINAL ALERT!
        //displayAlertMessage(message: "Almost there", action: segue(identifier: "toRegProPic"))

    }
    
    //NEXT BUTTON
    @IBAction private func NextButtonTapped(_ sender: UIButton) {
        checkEntries()
        segue(identifier: "toRegProPic")
    }
    
    @IBAction func loginButtonTapped(_ sender: UIButton) {
        segue(identifier: "regToLogin")
    }
    
    //CALL SEGUE
    func segue(identifier: String){
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: identifier, sender: self)
        }
    }
    
}

//EXTENTION LOCATION INFORMATION
extension RegisterPageViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            print(location.coordinate)  
            locationManager.stopUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            locationManager.startUpdatingLocation()
        }
    }
    
    //GET USERS COUNTRY
    func get_location(){
        let lcation = locationManager.location
        CLGeocoder().reverseGeocodeLocation(lcation!, completionHandler: {(placemarks, error) -> Void in
            
            if error != nil {
                print("Reverse geocoder failed with error" + (error?.localizedDescription)!)
                return
            }
            
            if (placemarks?.count)! > 0 {
                let pm = (placemarks?[0])! as CLPlacemark
                let homeTowntext = "\(pm.addressDictionary!["CountryCode"]!), \(pm.addressDictionary!["Country"]!)"
                self.countryTextField.text = homeTowntext
                self.areaCodeTextField.text = self.phoney.countryDictionary[pm.addressDictionary!["CountryCode"]! as! String]!
            }
            else {
                print("Problem with the data received from geocoder")
            }
        })
    }
}

