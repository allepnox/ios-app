//
//  SplashViewController.swift
//  Social-Local
//
//  Created by Epnox on 12/04/2017.
//  Copyright © 2017 Epnox. All rights reserved.
//

import UIKit

class SplashViewController: UIViewController {
    //VARIABLES
    var email = String()
    
    
    //INSTANCES
    let NetworkController = APIController()
    let AnimateController = AnimationsViewController()
    let Cache             = CacheController()
    
    
    /*----------------------------------------------------------START SWIFT FUNCTIONS-------------------------------------------------------------------------------------------*/
    //VIEW LOADED INTO MEMORY
    override func viewDidLoad() {
        // Do any additional setup after loading the view.
    
    }

    //VIEW APPEARS ON USER SCREEN
    override func viewDidAppear(_ animated: Bool) {
        //START ANIMATION
        view.addSubview(AnimateController.indicatorStart())
        
        let result = Cache.checkUserAvaibility()
        
        if result {
            if (user.email == nil) {
                self.performSegue(withIdentifier: "loginView", sender: self)
            }else{
                //CHECK SESSION KEY VALIDITY
                self.email = user.email!
                validate_Session_Key()
            }
    
            
        }else{
            self.performSegue(withIdentifier: "loginView", sender: self)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "loginView"{
            self.AnimateController.indicatorStop()
            if let loginVC : LoginViewController = segue.destination as? LoginViewController{
                loginVC.emailGlobal = self.email
            }
        }
    }
    
    //USER HAS LIMMITED MEMORY
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    /*----------------------------------------------------------END SWIFT FUNCTIONS-------------------------------------------------------------------------------------------*/
    
    
    /*----------------------------------------------------------START CACHE FUNCTIONS-------------------------------------------------------------------------------------------*/
    //UNPACK THE CACHE AND INITIALIZE VARIABLES
    private func unpackCache() {

        let listKey = Cache.getkeys()
        
        if listKey!.contains("Session"){
            
            var cacheSession = Cache.getDictionary(key: "Session")
            
            if cacheSession["avaibility"] != nil { print("Struct Session : Not Cache") }
            else{
                
                let _ = userSession(
                    accessKey: cacheSession["accessKey"] as! String,
                    socialNetworkType: cacheSession["socialNetworkType"] as! String,
                    firebaseKey: cacheSession["firebaseKey"] as! String
                )
            }
        }
    
    
        if listKey!.contains("Firebase"){
            
            let cacheFireBase = Cache.getDictionary(key: "Firebase")
            
            if cacheFireBase["avaibility"] != nil { print("Struct Firebase : Not Cache") }
            else{
                
                let _ = firebase(fire_Session_Key: cacheFireBase["fire_Session_Key"] as! String)
            }
        }
        
        if listKey!.contains("Profile"){
            
            let cacheProfile = Cache.getDictionary(key: "Profile")
            
            if cacheProfile["avaibility"] != nil { print("Struct Profile : Not Cache") }
            else{
                
                let _ = profile(
                    remaining_coin  : cacheProfile["remaining_coin"] as! String,
                    phone           : cacheProfile["phone_no"] as! String,
                    follower_count  : cacheProfile["follower_count"] as! Int,
                    image_src       : cacheProfile["image_src"] as! String,
                    gender          : cacheProfile["gender"] as! String,
                    date_of_birth   : cacheProfile["date_of_birth"] as! Double,
                    post_count      : cacheProfile["post_count"] as! Int,
                    username        : cacheProfile["username"] as! String,
                    hometown        : cacheProfile["hometown"] as! String
                )
            }
        }
        
        if listKey!.contains("Map"){
            
            let cacheMap = Cache.getDictionary(key: "Map")
            
            if cacheMap["avaibility"] != nil { print("Struct Map : Not Cache") }
            else{
                
                let _ = map(
                    lat         : cacheMap["lat"] as! Double,
                    long        : cacheMap["long"] as! Double,
                    radius      : cacheMap["radius"] as! Double,
                    sessionKey  : cacheMap["sessionKey"] as! String,
                    size        : cacheMap["size"] as! String
                )
            }
        }
        
        if listKey!.contains("User"){
            
            let cacheMap = Cache.getDictionary(key: "User")
            
            if cacheMap["avaibility"] != nil { print("Struct Map : Not Cache") }
            else{
                
                let _ = user.init(key: cacheMap["sessionKey"] as? String, id: cacheMap["userID"] as? String)
                let _ = user.init(email: cacheMap["email"] as? String, password: nil)
        }
    }
}
    /*----------------------------------------------------------END CACHE FUNCTIONS-------------------------------------------------------------------------------------------*/
    
    
    /*----------------------------------------------------------START HTTP FUNCTIONS--------------------------------------------------------------------------------------*/
    //CHECK IF USER SESSIONKEY IS STILL VALID ON SERVER
    private func validate_Session_Key(){
        NetworkController.getRequest(purpose: "validate_session_key"){
            data in
            if let sessionStatus = data["status"]{
                if String(describing: sessionStatus) == "1"{
                    self.unpackCache()
                    DispatchQueue.main.async{
                        self.AnimateController.indicatorStop()
                        self.performSegue(withIdentifier: "toHome", sender: self)
                    }
                }else{
                    //print some kind of error
                    DispatchQueue.main.async{
                        self.AnimateController.indicatorStop()
                        self.performSegue(withIdentifier: "loginView", sender: self)
                    }
                }
            }else{
                //print some kind of error
                DispatchQueue.main.async{
                    self.AnimateController.indicatorStop()
                    self.performSegue(withIdentifier: "loginView", sender: self)
                }
            }
        }
    }
    /*----------------------------------------------------------END HTTP FUNCTIONS-------------------------------------------------------------------------------------------*/
}
