//
//  SettingControllerViewController.swift
//  Social-Local
//
//  Created by Muhammad Nabil Hairul Fadilah on 31/03/2017.
//  Copyright © 2017 Epnox. All rights reserved.
//

import UIKit
import GooglePlaces

extension SettingController: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        var counting = 0
        
        let place = place.formattedAddress?.components(separatedBy: ", ")
        let count = (place?.count)!-1
        
        for text in place!{
            counting += 1
            if counting >= count{
                hometownSearch = hometownSearch + " " + text
            }
        }
        
        profile.hometown = self.hometownSearch
        hometown.text = profile.hometown
        hometownSearch = ""
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}

class SettingController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate
{
    //object from main.storyboard
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var hometown: UITextField!
    @IBOutlet weak var phone_number: UITextField!
    @IBOutlet weak var date_of_birth: UITextField!
    @IBOutlet weak var gender: UITextField!
    @IBOutlet weak var boxView: UIView!
    
    //setup variable (date formatter, date picker, picker view, toolbar)
    let dateFormatter = DateFormatter()
    let datePicker = UIDatePicker()
    let picker = UIPickerView()
    let toolbar_dob = UIToolbar()
    let toolbar_gender = UIToolbar()
    var alertController = UIAlertController()
    
    // variable that hold empty variable and value
    let genderArray = ["Male","Female","Others"]
    var selectedGender = String()
    var updatedProfile = UIImage()
    var selectedDOB = Date()
    var hometownSearch = String()
    
    // when view been loaded, this function will running
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        // this function to call setupview for setup view on update profile
        setupView()
        
        //fill a area needed such as : image,name,phone,dob,hometown
        if !(profile.image_src?.isEmpty)!{
            APIController().getImage(url: profile.image_src!){image in
                self.profileImage.image = image
            }
        }
        username.text = profile.username
        hometown.text = profile.hometown
        let DOB = DateController().unixToDate(profile.date_of_birth!)
        date_of_birth.text = DateController().dateToString(DOB)
        genderSelection(gender: profile.gender!)
        phone_number.text = "+60\(profile.phone!)"
        
        dateFormatter.dateFormat = "dd-MM-YYYY"
    }
    
    //setup view function
    func setupView(){
        
        toolbar()
        actionSheetView()
        
        profileImage.layer.cornerRadius = profileImage.frame.size.width / 2
        profileImage.clipsToBounds = true
        
        boxView.layer.cornerRadius = 10
        boxView.clipsToBounds = true
        
        dateFormatter.dateFormat = "dd/MM/yyyy"

    }
    
    //function to create action sheet to open camera / photo library
    func actionSheetView()
    {
        alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let cameraAction = UIAlertAction(title: "Camera", style: .default, handler: { action in
                self.openCamera()
        })
        let libraryAction = UIAlertAction(title: "Photo Library", style: .default, handler: { action in
                self.openLibrary()
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
        
        alertController.addAction(cameraAction)
        alertController.addAction(libraryAction)
        alertController.addAction(cancelAction)
    }
    
    //function for date of birth and gender to select the value
    func toolbar(){
        datePicker.datePickerMode = .date
        datePicker.maximumDate = Calendar.current.date(byAdding: .year, value: 0, to: Date())
        
        picker.delegate = self
        picker.dataSource = self
        
        toolbar_dob.sizeToFit()
        toolbar_gender.sizeToFit()
        
        let doneButton_dob = UIBarButtonItem(barButtonSystemItem: .done, target : nil, action: #selector(buttonDone_dob))
        let cancelButton_dob = UIBarButtonItem(barButtonSystemItem: .cancel, target : nil, action: #selector(buttonClose))
        let cancelButton_gender = UIBarButtonItem(barButtonSystemItem: .cancel, target : nil, action: #selector(buttonClose))
        let doneButton_gender = UIBarButtonItem(barButtonSystemItem: .done, target : nil, action: #selector(buttonDone_gender))
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        
        cancelButton_dob.tintColor = UIColor.red
        cancelButton_gender.tintColor = UIColor.red
        
        toolbar_dob.setItems([doneButton_dob,flexibleSpace,cancelButton_dob], animated: true)
        toolbar_gender.setItems([doneButton_gender,flexibleSpace,cancelButton_gender], animated: true)
        
        date_of_birth.inputAccessoryView =  toolbar_dob
        date_of_birth.inputView = datePicker
        gender.inputAccessoryView = toolbar_gender
        gender.inputView = picker
        
        datePicker.date = DateController().unixToDate(profile.date_of_birth!)

    }
    
    //due to in db can save "M/F/O" || "0/1", so need to change value to display
    func genderSelection(gender:String)
    {
        if gender == "1" || gender == "M" || gender == "Male" {
            self.gender.text = "Male"
            self.selectedGender = "M"
            picker.selectRow(0, inComponent: 0, animated: true)
        }else if gender == "2" || gender == "F" || gender == "Female"{
            self.gender.text = "Female"
            self.selectedGender = "F"
            picker.selectRow(1, inComponent: 0, animated: true)
        }else{
            self.gender.text = "Others"
            self.selectedGender = "O"
            picker.selectRow(2, inComponent: 0, animated: true)
        }
        
    }
    
    //button done for date of birth at toolbar
    func buttonDone_dob()
    {
        dateFormatter.dateFormat = "dd-MM-YYYY"
        self.selectedDOB = datePicker.date
        let dob = dateFormatter.string(from: datePicker.date)
        self.date_of_birth.text = dob
        profile.date_of_birth = DateController().dateToUnix(datePicker.date)
        self.view.endEditing(true)
    }
    
    //button done for gender at toolbar
    func buttonDone_gender(){
        self.gender.text = self.selectedGender
        self.view.endEditing(true)
    }
    
    //button close at toolbar
    func buttonClose()
    {
        self.view.endEditing(true)
    }
    
    //this is for gender picker function : number of components
    func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
        return 1
    }
    
    //this is for gender picker function : number of rows
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        return genderArray.count
    }
    
    //this is for gender picker function : title for row
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        return genderArray[row]
    }
    
    //this is for gender picker function : selecting row when appear
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        self.selectedGender = self.genderArray[row]
        
    }
    
    //handler image that been selected in library or been capture
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        let getImage = info[UIImagePickerControllerEditedImage] as! UIImage
        profileImage.image = getImage
        
        uploadProfileImage(image: getImage)
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func uploadProfileImage(image:UIImage){
        
        //Now use image to create into NSData format
        let imageData = UIImageJPEGRepresentation(image, 0.2)
    
        let strBase64 = imageData?.base64EncodedString()
        
        let image:[String:AnyObject] = [
            "userid"        : user.userID! as AnyObject,
            "session_key"   : user.sessionKey! as AnyObject,
            "image"         : strBase64 as AnyObject
        ]
        
        let headers = [String:String]()
        APIController().postRequest(purpose: "update_user_profile", parameter: image, headers: headers){ result in
            
        }
    }
    
    //button change profile image triggered
    @IBAction func changeProfileImage(_ sender: Any)
    {
        self.present(alertController, animated: true, completion: nil)
    }
    
    // Present the Autocomplete view controller when the button is pressed.
    @IBAction func autocompleteClicked(_ sender: UIButton) {
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        present(autocompleteController, animated: true, completion: nil)
    }
    
    //function to open camera
    func openCamera()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)
        {
            let imageCamera = UIImagePickerController()
            imageCamera.delegate = self
            imageCamera.sourceType = UIImagePickerControllerSourceType.camera
            imageCamera.allowsEditing = true
            self.present(imageCamera, animated: true, completion: nil)
            
        }else{
            print("cannot open camera")
        }
    }
    
    // function to open photo library
    func openLibrary()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary)
        {
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self
            myPickerController.sourceType = UIImagePickerControllerSourceType.photoLibrary
            myPickerController.allowsEditing = true
            
            self.present(myPickerController, animated: true, completion: nil)
        }else
        {
            print ("cannot open library")
        }
    }
    
    @IBAction func BackButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
 
    @IBAction func DOBTriggered(_ sender: Any) {
        datePicker.setDate(self.selectedDOB, animated: true)
    }
    
    @IBAction func genderTriggered(_ sender: Any) {
        genderSelection(gender: self.gender.text!)
    }
    
    @IBAction func cancelButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func saveButton(_ sender: Any) {
        
        let data: [String:AnyObject] = [
            "userid"        : user.userID! as AnyObject,
            "session_key"   : user.sessionKey! as AnyObject,
            "hometown"      : self.hometown.text! as AnyObject,
            "dob"           : profile.date_of_birth as AnyObject,
            "gender"        : self.selectedGender as AnyObject
        ]
        
        let headers = [String:String]()
        APIController().postRequest(purpose: "update_user_profile", parameter: data, headers: headers){ result in
            print(result)
        }
        
        profile.gender = self.selectedGender
        
        let alert = UIAlertController(title: "Success", message: "Profile Updated", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: cancelButton))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    

}





























