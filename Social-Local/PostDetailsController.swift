//
//  postDetails.swift
//  Social-Local
//
//  Created by Muhammad Nabil Hairul Fadilah on 13/04/2017.
//  Copyright © 2017 Epnox. All rights reserved.
//

import UIKit

class PostDetailsController: UIViewController {
    
    //MARK - Object
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var ownerName: UILabel!
    @IBOutlet weak var postImage: UIImageView!
    @IBOutlet weak var timestamp: UILabel!
    @IBOutlet weak var location: UILabel!
    @IBOutlet weak var Caption: UILabel!
    @IBOutlet weak var numberLikes: UILabel!
    @IBOutlet weak var viewArea: UIView!
    @IBOutlet weak var LikeButton: UIButton!
    @IBOutlet weak var ChatButton: UIButton!
    
    //MARK - Variable
    var data = [String:AnyObject]()
    
    //MARK - Override Function
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        
         ownerName.text = profile.username
         postImage.image = data["image_src"] as? UIImage
         let date = NSDate(timeIntervalSince1970: data["timestamp"] as! TimeInterval)
         timestamp.text = "\(date)"
         location.text = data["location"] as? String
         Caption.text = data["caption"] as? String
         numberLikes.text = data["likes"] as? String
        
        if let image_src = profile.image_src {
            DispatchQueue.main.async {
                APIController().getImage(url: image_src){ image in
                    self.profileImage.image = image
                }
            }
        }
        if let urlImage = data["image"] as? String {
            DispatchQueue.main.async {
                APIController().getImage(url: urlImage){ image in
                    self.postImage.image = image
                }
            }
        }
        
 
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "chatSegue"{
            // create code to transfer to chat controller
            // userid,chatid. and if needed include the image
        }
    }
    
    //MARK - Function Section
    func setupView(){
        
        profileImage.layer.cornerRadius = profileImage.frame.size.width / 2
        profileImage.clipsToBounds = true
        
        viewArea.layer.cornerRadius = 10
        viewArea.clipsToBounds = true
    }
    
    // MARK - Button Action Section
    @IBAction func outsideAreaButton(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func closeButton(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func chatButton(_ sender: Any) {
        
        performSegue(withIdentifier: "chatSegue", sender: nil)
    }
    
    @IBAction func reportButton(_ sender: Any) {
        
        let parameter : [String:AnyObject] = [
            "userid"        : user.userID as AnyObject,
            "imageid"       : data["image_id"] as AnyObject,
            "reasons"       : "inappropiate image !" as AnyObject,
            "session_key"   : user.sessionKey  as AnyObject
        ]
        let headers = [String:String]()
        APIController().postRequest(purpose: "report_image", parameter: parameter, headers: headers){ data in
            print(data)
        }
    }
    @IBAction func likesButton(_ sender: Any) {
        
        let parameter = [
            "userid"        : user.userID!,
            "imageid"       : data["image_id"]!,
            "session_key"   : user.sessionKey!
        ] as [String : AnyObject]
        
        let headers = [String:String]()
        APIController().postRequest(purpose: "insert_likes", parameter: parameter, headers: headers){
            request in
            print (request)
            
            self.LikeButton.setImage(UIImage(named: "likes"), for: .normal )
            
        }
        
    }
}















