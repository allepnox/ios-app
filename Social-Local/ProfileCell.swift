//
//  ProfileCell.swift
//  Social-Local
//
//  Created by Muhammad Nabil Hairul Fadilah on 24/03/2017.
//  Copyright © 2017 Epnox. All rights reserved.
//

import UIKit

class ProfileCell: UITableViewCell {
    
    // for followerController
    @IBOutlet weak var photoFollower: UIImageView!
    @IBOutlet weak var nameFollower: UILabel!
    
    // for chatController
    @IBOutlet weak var nameChat: UILabel!
    @IBOutlet weak var photoChat: UIImageView!
    @IBOutlet weak var messageChat: UILabel!
    @IBOutlet weak var timeStampChat: UILabel!
    
    // for notificationController
    @IBOutlet weak var imageNotification: UIImageView!
    @IBOutlet weak var titleNotification: UILabel!
    @IBOutlet weak var messageNotification: UILabel!
    @IBOutlet weak var timestampNotification: UILabel!
    
    //for ShopController
    @IBOutlet weak var priceCoin: UIButton!
    @IBOutlet weak var numberCoin: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
   
}
