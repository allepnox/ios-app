//
//  SegueController.swift
//  Social-Local
//
//  Created by Epnox on 02/05/2017.
//  Copyright © 2017 Epnox. All rights reserved.
//

import UIKit

//Segue Functions I am tired of reacreating
class SegueController: UIViewController {

    //CALL SEGUE
    func segue(identifier: String){
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: identifier, sender: self)
        }
    }

}
