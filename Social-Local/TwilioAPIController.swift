//
//  TwilioAPIController.swift
//  Social-Local
//
//  Created by Remigius Kalimba on 02/05/2017.
//  Copyright © 2017 Epnox. All rights reserved.
//

import UIKit
import FirebaseAuth
import Firebase

class TwilioAPIController: UIViewController {
    
    
    //VARIABLES
    private let API_CODE: String = "NlngcmlwnNvwuZfGhfUVSO6ulRBhWS05"
    @IBOutlet weak var smsKeyTextField: UITextField!
    @IBOutlet weak var countDownLabel: UILabel!
    @IBOutlet weak var resendButton: UIButton!
    @IBOutlet weak var verifyButton: UIButton!
    
    
    //FROM REGPICCONTROLLER
    var email               = String()
    var password            = String()
    var phoneNumber         = String()
    var DOB                 = Double()
    var gender              = String()
    var country             = String()
    var areaCode            = String()
    var firebaseChatId      = String()
    var firebaseChatEntity  = String()
    var getImage            : UIImage?
    
    
    //INSTANCES
    let NetworkController       = APIController()
    let GlobalFuncController    = GlobalFuncsController()
    let AnimateController       = AnimationsViewController()
    let Cache                   = CacheController()
    
    
    override func viewDidAppear(_ animated: Bool) {
        let _ = twilio.init(phone_number      : self.phoneNumber,
                            country_code      : self.areaCode,
                            locale            : "en",
                            email             : self.email)
        
        requestCode()
    }
    
    
    override func viewDidLoad() {
        twilio.atRegisterFlag = true
        
        print(country)
        print(gender)
        print(getImage!)
        super.viewDidLoad()
        //DISMISS KEYBOARD
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(RegisterPageViewController.dismissKeyboard))
        self.view.addGestureRecognizer(tap)
        
        //START COUNTER
        self.countDownLabel.text = String(describing: self.timerCount)
        self.resendButton.isEnabled = false
        self.resendButton.backgroundColor = UIColor.gray
        var _ = Timer.scheduledTimer(timeInterval: 1, target: self, selector:#selector(TwilioAPIController.countDownTimer), userInfo: nil, repeats: true)
    }
    
    
    //DISMISSKEYBOARD
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    
    //COUNTDOWN
    var timerCount = 60
    func countDownTimer(){
        if(timerCount > 0){
            timerCount -= 1
            self.resendButton.isEnabled = false
            self.resendButton.backgroundColor = UIColor.gray
            self.countDownLabel.text = String(describing: timerCount)
        }else{
            resendButton.isEnabled = true
            self.resendButton.backgroundColor = UIColor.orange
        }
    }
    
    
    //BUTTON
    @IBAction func verifyButtonTapped(_ sender: UIButton) {
        if(!(smsKeyTextField.text?.isEmpty)!){
            print("Check Code")
            self.checkCode()
            
        }else{
            let alert = self.GlobalFuncController.displayAlertMessage(title: "Alert", buttonTittle: "OK", message: "Please insert verification code", action: self.GlobalFuncController.defaultAction())
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    
    //BUTTON
    @IBAction func resendButtonTapped(_ sender: UIButton) {
        if(self.timerCount <= 0 && self.resendButton.isEnabled == true){
            self.timerCount = 60
            self.requestCode()
        }else{
            let alert = self.GlobalFuncController.displayAlertMessage(title: "Alert", buttonTittle: "OK", message: "Please wait \(timerCount) seconds before requesting another code", action: self.GlobalFuncController.defaultAction())
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    
    //TWILIO
    func requestCode(){
        let params = [
            "api_key"               : API_CODE,
            "via"                   :"sms",
            "phone_number"          : self.phoneNumber,
            "country_code"          : self.areaCode
            ] as [String: AnyObject]
        
        let headers = [String:String]()
        
        //NETWORK REQUEST
        NetworkController.postRequest(purpose: "twilioStart", parameter: params as Dictionary<String, AnyObject>, headers: headers){
            data in
            if(data["success"]! as! Bool == true){
                let alert = self.GlobalFuncController.displayAlertMessage(title: "Alert", buttonTittle: "OK", message: "Verification code sent via sms", action: self.GlobalFuncController.defaultAction())
                self.present(alert, animated: true, completion: nil)
            }else{
                print("There is an error!")
            }
        }
    }
    
    
    //TWILIO
    func checkCode(){
        self.verifyButton.isEnabled = false
        self.verifyButton.backgroundColor = UIColor.gray
        view.addSubview(AnimateController.indicatorStart())
        
        if let _ = self.smsKeyTextField.text{
            let _ = twilio.init(verification_code : self.smsKeyTextField.text!)
        }else{
            print("Error setting up struct")
        }
        
        //NETWORK REQUEST
        NetworkController.getRequestNoEncoding(purpose: "twilioCheck"){
            data in
            print(data)
            if(data["success"]! as! Bool == true){
                let _ = user.init(email: self.email,password: self.password)
                self.fireBaseSignUp()
            }else{
                let _ = self.GlobalFuncController.displayAlertMessage(title: "Alert", buttonTittle: "OK", message: "This code has expires. please request a new code.", action: ())
                self.smsKeyTextField.text = String()
                self.smsKeyTextField.attributedPlaceholder = NSAttributedString(string: "Invalid Key!", attributes: [NSForegroundColorAttributeName: UIColor.red])
            }
        }
        
        self.verifyButton.isEnabled = true
        self.verifyButton.backgroundColor = UIColor.orange
        AnimateController.indicatorStop()
    }
    
    
    //SEND USER DETAILS ARRAY
    private func postToSocialLocal(){
        let imageData = UIImageJPEGRepresentation(self.getImage!, 0.2)
        let strBase64 = imageData?.base64EncodedString(options: .lineLength64Characters)
        let data:[String:AnyObject] = [
            "username"          : email as AnyObject,
            "password"          : password as AnyObject,
            "phoneno"           : phoneNumber as AnyObject,
            "dob"               : DOB as AnyObject,
            "gender"            : gender as AnyObject,
            "hometown"          : country as AnyObject,
            "countrycode"       : areaCode as AnyObject,
            "chatid"            : self.firebaseChatId as AnyObject,
            "chatentity"        : self.firebaseChatEntity as AnyObject,
            "image"             : strBase64 as AnyObject
        ]
        let headers = [String:String]()
        
        NetworkController.postRequest(purpose: "user_resgistration", parameter: data, headers: headers){
            data in
            print(data)
            //["status": 1, "chat_id": , "user_id": 126, "chat_entity_id": , "session_key": c3fb61aa3479eab|126]
            if !data.isEmpty{
                //needed to request for user profile
                if let sessionKey = data["session_key"]{
                    let key = sessionKey as! String
                    let id = data["user_id"]! as! Int
                //Handle data check
                let _ = user.init(key: key, id: String(describing: id))
                }
                self.unpackProfile()
            }else{
                print("unable return value", "probably internet not working :)")
                let _ = self.GlobalFuncController.displayAlertMessage(title: "Error", buttonTittle: "OK", message: "Check Internet Connecetion", action: ())
                print(data)
            }
        }
    }
    
    
    private func fireBaseSignUp(){
        let fireEmail = self.email
        let firePassword = self.password
        
        FIRAuth.auth()?.createUser(withEmail: fireEmail, password: firePassword, completion: { (user, error) in
            if error == nil{
                self.firebaseChatId = (user?.providerID)!
                self.firebaseChatEntity = (user?.uid)!
                print(self.firebaseChatId)
                print(self.firebaseChatEntity)
                self.postToSocialLocal()
                
            }else{
                //There has been some kind of error, need to handle it
                print("Error")
            }
        })
    }
    
    
    private func unpackProfile(){
        NetworkController.getRequest(purpose: "get_user_profile"){data in
            if !data.isEmpty{
            var image_src = String()
            
            if !(data["image_src"] is NSNull){
                image_src = data["image_src"] as! String
            }
            
            let _ = profile(
                remaining_coin: data["remaining_coin"] as! String,
                phone         : data["phone_no"]       as! String,
                follower_count: data["follower_count"] as! Int,
                image_src     : image_src,
                gender        : data["gender"]         as! String,
                date_of_birth : data["date_of_birth"]  as! Double,
                post_count    : data["post_count"]     as! Int,
                username      : data["username"]       as! String,
                hometown      : data["hometown"]       as! String
            )
            
            var storeValue : [String:AnyObject] = [:]
            
            for (key,value) in data{
                if value is NSNull {
                    storeValue[key] = "empty" as AnyObject
                }else{
                    storeValue[key] = value
                }
            }
            
            self.Cache.setCache(value: storeValue, key: "Profile")
            self.Cache.sync()
            //Maybe put up a couple flags and do a check
                DispatchQueue.main.async {
                    self.doSegue(destination: "twilioToNavController")
                }
                
            }else{
                print("unpck profile data dictionary is empty")
            }
    }
    }
    
    
    private func doSegue(destination: String){
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: destination, sender: self)
        }
    }
    
}
