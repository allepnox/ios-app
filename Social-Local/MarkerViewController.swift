//
//  MarkerViewController.swift
//  
//
//  Created by Epnox on 21/04/2017.
//
//

import UIKit

@IBDesignable class MarkerViewController: UIView{
    
    var view:UIView!
    @IBOutlet weak var markerImage: UIImageView!
    
    @IBInspectable var MapImageIcon:UIImage?{
        get{
            return markerImage.image
        }
        
        set(MapImageIcon){
            markerImage.image = MapImageIcon
        }
    }
    
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setup(){
        view = laodViewFromNib()
        view.frame = bounds
        addSubview(view)
    }
    
    func laodViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib.init(nibName: "MarkerViewController", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        
        return view
    }
}
