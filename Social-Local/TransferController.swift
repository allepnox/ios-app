//
//  TransferController.swift
//  Social-Local
//
//  Created by Muhammad Nabil Hairul Fadilah on 21/04/2017.
//  Copyright © 2017 Epnox. All rights reserved.
//
//  Class : To transfer all data from database to struct and cache

import UIKit

class TransferController{
    
    let api = APIController()
    let Cache = CacheController()
    
    func Profile(){
    
        api.getRequest(purpose: "get_user_profile"){data in
            
            var image_src = String()
            
            if !(data["image_src"] is NSNull){
                image_src = data["image_src"] as! String
            }
            
            let userProfile = profile(
                remaining_coin: data["remaining_coin"] as! String,
                phone         : data["phone_no"]       as! String,
                follower_count: data["follower_count"] as! Int,
                image_src     : image_src,
                gender        : data["gender"]         as! String,
                date_of_birth : data["date_of_birth"]  as! Double,
                post_count    : data["post_count"]     as! Int,
                username      : data["username"]       as! String,
                hometown      : data["hometown"]       as! String
            )
            
            self.Cache.setCache(value: userProfile, key: "Profile")
            self.Cache.sync()
        }

    }
}

