//
//  GQuadItem.swift
//  google-maps-ios-utils-swift
//
//  Created by Eyal Darshan on 24/05/2016.
//  Copyright © 2016 eyaldar. All rights reserved.
//

import CoreLocation
import Foundation

final class GQuadItem: NSObject, GCluster, GQTPointQuadTreeItem, NSCopying {
    fileprivate var _item: GClusterItem
    fileprivate var _point: GQTPoint
    fileprivate var _position: CLLocationCoordinate2D
    fileprivate var _imageSRC: String
    
    /**
     * Controls whether this marker will be shown on map.
     */
    var hidden: Bool = false
    
    var name: String {
        return _item.name
    }
    
    var imageSRC: String{
        return _item.imageSRC
    }
    
    var item: GClusterItem {
        return _item
    }
    
    var items: NSSet {
        return NSSet(object: _item)
    }
    
    var position: CLLocationCoordinate2D {
        return _position
    }
    
    var point: GQTPoint {
        return _point
    }
    
    init(clusterItem: GClusterItem) {
        let projection = SphericalMercatorProjection(worldWidth: 1.0)
        
        _position = clusterItem.location
        _point = projection.coordinateToPoint(_position)
        _item = clusterItem
        _imageSRC = clusterItem.imageSRC
    }
    
    func copy(with zone: NSZone?) -> Any {
        let newGQuadItem = GQuadItem(clusterItem: _item)
        newGQuadItem._point = _point
        newGQuadItem._item = _item
        newGQuadItem._position = _position
        newGQuadItem._imageSRC = _imageSRC
        return newGQuadItem
    }
    
     /*override func isEqual(_ other: Any?) -> Bool {
        if self.isEqual(other) {
            return true
        }
        
        guard let `other` = other as? GQuadItem else {
            return false
        }
        
        return self._item.isEqual(other._item)
    }*/
    
    override var hash: Int {
        return _item.hash
    }
}

