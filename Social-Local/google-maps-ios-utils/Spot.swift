//
//  Spot.swift
//  google-maps-ios-utils-swift
//
//  Created by Eyal Darshan on 24/05/2016.
//  Copyright © 2016 eyaldar. All rights reserved.
//

import CoreLocation
import Foundation
import GoogleMaps

class Spott: NSObject, GClusterItem {
    let name: String
    let location: CLLocationCoordinate2D
    let  imageSRC: String
    
    init(name: String, location: CLLocationCoordinate2D, imageSRC:String) {
        self.name = name
        self.location = location
        self.imageSRC = imageSRC
    }
}
