//
//  GStaticCluster.swift
//  google-maps-ios-utils-swift
//
//  Created by Eyal Darshan on 24/05/2016.
//  Copyright © 2016 eyaldar. All rights reserved.
//

import CoreLocation
import Foundation

final class GStaticCluster: GCluster {
    fileprivate var _position: CLLocationCoordinate2D
    fileprivate var _imageSRC: String
    fileprivate var _items: Set<GQuadItem>
    
    var items: NSSet {
        return _items as NSSet
    }
    
    var position: CLLocationCoordinate2D {
        if let quadItem = _items.first, _items.count == 1 {
            return quadItem.item.location
        }
        
        return _position
    }
    
    var imageSRC: String {
        if let quadItem = _items.first, _items.count == 1 {
            return quadItem.item.imageSRC
        }
        
        return _imageSRC
    }

    
    init(coordinate: CLLocationCoordinate2D, imageSRC: String) {
        _position = coordinate
        _imageSRC = imageSRC
        _items = Set<GQuadItem>()
    }
    
    func add(_ item: GQuadItem) {
        _items.insert(item)
    }
    
    func remove(_ item: GQuadItem) {
        _items.remove(item)
    }
}
