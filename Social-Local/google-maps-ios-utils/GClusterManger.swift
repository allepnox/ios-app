//
//  GClusterManger.swift
//  google-maps-ios-utils-swift
//
//  Created by Eyal Darshan on 24/05/2016.
//  Copyright © 2016 eyaldar. All rights reserved.
//

import Foundation
import GoogleMaps
import UIKit

final class GClusterManger: NSObject {
    fileprivate var _previousCameraPosition: GMSCameraPosition?
    weak var delegate: GMSMapViewDelegate?
    
    var isZoomDependent: Bool = false
    var items: NSMutableArray?
    
    var mapView: GMSMapView {
        didSet {
            _previousCameraPosition = nil
        }
    }
    
    var clusterAlgorithm: GClusterAlgorithm {
        didSet {
            _previousCameraPosition = nil
        }
    }
    
    var clusterRenderer: GClusterRenderer {
        didSet {
            _previousCameraPosition = nil
        }
    }
    
    init(mapView: GMSMapView, algorithm: GClusterAlgorithm, renderer: GClusterRenderer) {
        self.mapView = mapView
        self.clusterAlgorithm = algorithm
        self.clusterRenderer = renderer
    }
    
    func addItem(_ item: GClusterItem) {
        clusterAlgorithm.addItem(item)
    }
    
    func removeItems() {
        clusterAlgorithm.removeItems()
    }
    
    func removeItemsNotInRectangle(_ bounds: GQTBounds) {
        clusterAlgorithm.removeItemsNotInRectangle(bounds)
    }
    
    func hideItemsNotInBounds(_ bounds: GMSCoordinateBounds) {
        clusterAlgorithm.hideItemsNotInBounds(bounds)
    }
    
    func cluster() {
        //let priority = DispatchQueue.global(qos: .default)
        let zoom = Double(mapView.camera.zoom)
        
        DispatchQueue.global(qos: .default).async { [unowned self] in
            let clusters = self.clusterAlgorithm.getClusters(zoom)
            
            DispatchQueue.main.async { [unowned self] in
                self.clusterRenderer.clustersChanged(clusters)
                }
            }
        }
    }


extension GClusterManger: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        delegate?.mapView?(mapView, willMove: gesture)
    }
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {  
        delegate?.mapView?(mapView, didChange: position)
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        assert(mapView == self.mapView)
        
        if isZoomDependent {
            let visibleRegion = mapView.projection.visibleRegion()
            let gmsBounds = GMSCoordinateBounds(region: visibleRegion)
            
            self.hideItemsNotInBounds(gmsBounds)
        }
        
        // Don't recompute clusters if the map has just been panned/tilted/rotated.
        let pos = mapView.camera
        
        if _previousCameraPosition?.target.latitude == pos.target.latitude &&
           _previousCameraPosition?.target.longitude == pos.target.longitude {
                if delegate != nil {
                    delegate?.mapView?(mapView, idleAt: position)
                    return
                }
        }
        
        _previousCameraPosition = mapView.camera
        
        self.cluster()
        
        delegate?.mapView?(mapView, idleAt: position)
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        delegate?.mapView?(mapView, didTapAt: coordinate)
    }
    
    func mapView(_ mapView: GMSMapView, didCloseInfoWindowOf marker: GMSMarker) {
        delegate?.mapView?(mapView, didCloseInfoWindowOf: marker)
    }
    
    func mapView(_ mapView: GMSMapView, didBeginDragging marker: GMSMarker) {
        delegate?.mapView?(mapView, didBeginDragging: marker)
    }
    
    func mapView(_ mapView: GMSMapView, didLongPressInfoWindowOf marker: GMSMarker) {
        delegate?.mapView?(mapView, didLongPressInfoWindowOf: marker)
    }
    
    func mapView(_ mapView: GMSMapView, didLongPressAt coordinate: CLLocationCoordinate2D) {
        delegate?.mapView?(mapView, didLongPressAt: coordinate)
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        if let result = delegate?.mapView?(mapView, didTap: marker) {
            return result
        }
        
        return false
    }
    
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
        delegate?.mapView?(mapView, didTapInfoWindowOf: marker)
    }
    
    func mapView(_ mapView: GMSMapView, didTap overlay: GMSOverlay) {
        delegate?.mapView?(mapView, didTap: overlay)
    }
    
    func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
        if let view = delegate?.mapView?(mapView, markerInfoWindow: marker) {
            return view
        }
        
        return nil
    }
    
    func mapView(_ mapView: GMSMapView, markerInfoContents marker: GMSMarker) -> UIView? {
        if let view = delegate?.mapView?(mapView, markerInfoContents: marker) {
            return view
        }
        
        return nil
    }
    
    func mapView(_ mapView: GMSMapView, didEndDragging marker: GMSMarker) {
        delegate?.mapView!(mapView, didEndDragging: marker)
        //delegate?.mapView!(mapView, didEnd: marker)
    }
    
    func mapView(_ mapView: GMSMapView, didDrag marker: GMSMarker) {
        delegate?.mapView!(mapView, didDrag: marker)
    }
}
