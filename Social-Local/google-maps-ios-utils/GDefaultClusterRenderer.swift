//
//  GDefaultClusterRenderer.swift
//  google-maps-ios-utils-swift
//
//  Created by Eyal Darshan on 24/05/2016.
//  Copyright © 2016 eyaldar. All rights reserved.
//

import Foundation
import GoogleMaps

final class GDefaultClusterRenderer: GClusterRenderer {
    fileprivate let _mapView: GMSMapView
    fileprivate var _visibleMarkers: [GMSMarker]
    fileprivate var _markerStack: Stack<GMSMarker>
    fileprivate var _ImageSRC: String
    
    //Instances
    let NetworkController = APIController()
    
    init(mapView: GMSMapView) {
        _mapView = mapView
        _visibleMarkers = []
        _markerStack = Stack<GMSMarker>(max: 64)
        _ImageSRC = String()
    }
    
    func clustersChanged(_ clusters: NSSet) {
        clearCaches()
        
        for cluster in clusters {
            guard let `cluster` = cluster as? GStaticCluster else {
                continue
            }
            
            _ImageSRC = cluster.imageSRC
            let marker = getMarker()
            _visibleMarkers.append(marker)
            let count = cluster.items.count
            
            
            NetworkController.getImage(url: _ImageSRC){
                image in
                
                if count >= 1 {
                    self.createOrUpdateIconView(marker, count: count, image: image)
                }
                else if count == 1 {
                    marker.iconView = self.generateClusterIconWithCount(1, image: #imageLiteral(resourceName: "kid"))
                }
                
                marker.position = cluster.position
                marker.tracksViewChanges = false
                marker.map = self._mapView
                
            }
            
    
        }
    }
    
    fileprivate func generateClusterIconWithCount(_ count: Int, image:UIImage) -> UIView {
        return GDefaultClusterMarkerIconView(count: count, markerImage:image)
    }
    
    fileprivate func getMarker() -> GMSMarker {
        var marker: GMSMarker!
        
        if let recycledMarker = _markerStack.pop() {
            marker = recycledMarker
        } else {
            marker = GMSMarker()
        }
        
        return marker
    }
    
    fileprivate func createOrUpdateIconView(_ marker: GMSMarker, count: Int, image: UIImage) {
        if let iconView = marker.iconView as? GDefaultClusterMarkerIconView {
            iconView.update(count)
            marker.iconView = iconView
        } else {
            marker.iconView = generateClusterIconWithCount(count, image: image)
        }
    }
    
    fileprivate func clearCaches() {
        for marker in _visibleMarkers {
            marker.map = nil
            
            _markerStack.push(marker)
        }
        
        _visibleMarkers.removeAll()
    }
}
