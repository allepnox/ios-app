//
//  CacheController.swift
//  Social-Local
//
//  Created by Epnox on 18/04/2017.
//  Copyright © 2017 Epnox. All rights reserved.
//

import UIKit

class CacheController {
    //Instance
    let CacheDict = dump(Array(UserDefaults.standard.dictionaryRepresentation().keys))

    //set cache to synchronize
    func sync(){
        UserDefaults.standard.synchronize()
    }
    
    //returns a value for the given key in user cache
    func getObject(key: String)-> Any{
        
        if CacheDict.contains(key){
            return  UserDefaults.standard.object(forKey: key)!
        }
        return false
    }
    
    func getDictionary (key: String)->[String:Any]{
        if CacheDict.contains(key){
            return  UserDefaults.standard.dictionary(forKey: key)!
        }
        return ["availability":""]
    }
    
    func getArray(key:String) -> [Any]{
        if CacheDict.contains(key){
            return  UserDefaults.standard.array(forKey: key)!

        }
        return []
    }

    //set a value and key pair into Cache/UserDefaults
    func setCache(value: Any, key: String){
        if UserDefaults.standard.object(forKey: key) == nil{
            UserDefaults.standard.set(value, forKey: key)
        }else if UserDefaults.standard.object(forKey: key) != nil{
            UserDefaults.standard.removeObject(forKey: key)
            UserDefaults.standard.set(value, forKey: key)
        }else{
            //not sure what to do here yet

        }
    }
    
    //Returns an array of all the keys in the cache
    func getkeys()->[String]!{
        return self.CacheDict
    }
    
    
    
    func clearCache(){
        
        let keys = ["Session","Firebase","Profile","Map","Posts","Follower"]
        
        let value = [
            "sessionKey" : "",
            "userID"     : "",
            "email"      : user.email,
            "password"   : ""
            ] as! [String:String]
        
        setCache(value: value, key: "User")
        
        for key in keys{
            if CacheDict.contains(key){
                UserDefaults.standard.removeObject(forKey: key)
            }
        }
    }
    
    func checkUserAvaibility() -> Bool{
        
        guard let listKey:[String] = getkeys() else{
            print("unable to return list of keys in cache")
            return false
        }
        
            if listKey.contains("User"){
                
                guard let UserCache : [String:Any] = getDictionary(key: "User") else{
                    return false
                }
                if let sessionKey = UserCache["sessionKey"], let userID = UserCache["userID"], let userEmail = UserCache["userID"]{
                    let _ = user.init(key: sessionKey as? String , id: userID as? String)
                    let _ = user.init(email: userEmail as? String, password: nil)
                }
                if let sessionKey = UserCache["sessionKey"], let userID = UserCache["userID"]{
                    let _ = user.init(key: sessionKey as? String , id: userID as? String)
                    return true
                }else{
                    return false
                }
            }else{
                return false
        }
        }

    }
















