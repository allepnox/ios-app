//
//  map.swift
//  Social-Local
//
//  Created by Muhammad Nabil Hairul Fadilah on 22/03/2017.
//  Copyright © 2017 Epnox. All rights reserved.
//
//Take this file

import UIKit
import GoogleMaps

/*-----------------------------------------------------------------START MARKER CLASS-------------------------------------------------------------------------------------------*/
//CREATE MARKER OBJECT
class MarkerDetails: NSObject, GClusterItem {
    let name: String
    let location: CLLocationCoordinate2D
    let imageSRC: String
    
    init(name: String, location: CLLocationCoordinate2D, imageSRC: String) {
        self.name = name
        self.location = location
        self.imageSRC = imageSRC
    }
}
/*-------------------------------------------------------------------END MARKER CLASS-------------------------------------------------------------------------------------------*/


class MapViewController: UIViewController, GMSMapViewDelegate {
    //INSTANCES
    private let networkController = APIController()
    private let animate = AnimationsViewController()
    
    //PRIVATE VARIABLES
    private var markerDetailsArray = [MarkerDetails]()
    private var clusterManager:GClusterManger?
    
    //PUBLIC VARIABLES
    var mapView: GMSMapView?
    var LocationManager = CLLocationManager()
    var postedGPS = [String:Double]()
    
    //Temp
    var userLat: Double?
    var userLong: Double?
    var sessionkey: String?
    
    let kStartingLocation = CLLocationCoordinate2D(latitude: 51.503186, longitude: -0.126446)
    let kStartingZoom: Float = 9.5
    
/*-------------------------------------------------------------------- START SWIFT FUNCTIONS -----------------------------------------------------------------------------------*/

    override func viewDidLoad() {
        let indicator = animate.indicatorStart()
        self.view.addSubview(indicator)
        
            }
    
    override func viewDidAppear(_ animated: Bool) {
        LocationManager.delegate = self
        LocationManager.desiredAccuracy = kCLLocationAccuracyBest
        LocationManager.requestWhenInUseAuthorization()
        LocationManager.requestAlwaysAuthorization()
        locationManager(LocationManager, didChangeAuthorization: CLAuthorizationStatus.authorizedAlways)
        //locationManager(LocationManager, didChangeAuthorization: CLAuthorizationStatus.authorizedWhenInUse)
        locationManager(LocationManager, didUpdateLocations: [LocationManager.location!])
        
        
        self.animate.indicatorStop()
        
        //let cameraPosition = GMSCameraPosition.camera(withTarget: kStartingLocation, zoom: kStartingZoom)
        //mapView? = GMSMapView(frame: CGRect.zero)
        //mapView?.camera = cameraPosition
        //mapView?.isMyLocationEnabled = true
        //mapView?.settings.myLocationButton = true
        //mapView?.settings.compassButton = true
        //self.view = mapView
        
        let _ = map.init(
            lat: self.userLat!,
            long: self.userLong!,
            radius: 15,
            sessionKey: user.sessionKey!,
            size: "small"
        )

        self.makeMap(lat: kStartingLocation.latitude, long: kStartingLocation.longitude, zoom: kStartingZoom)
        clusterManager = GClusterManger(mapView: mapView!, algorithm: NonHierarchicalDistanceBasedAlgorithm(), renderer: GDefaultClusterRenderer(mapView: mapView!))
        clusterManager?.isZoomDependent = true
        mapView?.delegate = clusterManager
        clusterManager?.delegate = self as GMSMapViewDelegate
        
        
        
        //get images(JSON)
        self.networkController.getRequest(purpose: "get_images_by_gps"){
            data in
            self.getDetails(data: data)
            self.getMarkers()
            self.AnimateMap()
        }

    }
/*--------------------------------------------------------------------- END SWIFT FUNCTIONS ----------------------------------------------------------------------------------*/

    
/*----------------------------------------------------------------- START MAP REFRESH FUNCTIONS --------------------------------------------------------------------------------*/
    //PLAN 1: CLEAR MAP AND REDOWNLOAD IMAGES
    private func mapRefresh(){
        self.mapView?.clear()
        self.networkController.getRequest(purpose: "get_images_by_gps"){
            data in
            self.getDetails(data: data)
            self.makeMap(lat: -22.55941 , long: 17.08323, zoom: 10)
            //self.getMarkers()
        }
    }
    
    
/*------------------------------------------------------------------ END MAP REFRESH FUNCTIONS ---------------------------------------------------------------------------------*/


/*--------------------------------------------------------------------- START MAP FUNCTIONS ------------------------------------------------------------------------------------*/
    
    //MOTION ANIMATION
    func AnimateMap(){
        CATransaction.begin()
        CATransaction.setValue(4, forKey: kCATransactionAnimationDuration)
        self.mapView?.animate(to: GMSCameraPosition.camera(withLatitude: map.lat!, longitude: map.long!, zoom: 10))
        CATransaction.commit()
    }
    
    //USER START POINT(maek a map at the userslocation)
    private func makeMap(lat: Double, long: Double, zoom: Float){
        let camera = GMSCameraPosition.camera(withLatitude: map.lat!, longitude: map.long!, zoom: zoom)
        self.mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        view = self.mapView
    }
    
    //GETS LOCATION AND IMAGES
    private func getDetails(data: [String: AnyObject]){
        let data = data
        print(data)
        //GET POINTS
        var idNum = 0
        if let _ = data["results"] as? [AnyObject]{
            for list in data["results"] as! [AnyObject]{
                if let value = list["geometry"] as? [String:AnyObject]{
                    if let coords = value[["coordinates"][0]]! as? NSArray{
                        let markerLong = coords[0] as? Double
                        let markerLat = coords[1] as? Double
                        if !(list["properties"] is NSNull){
                            let imageProperties = list["properties"] as! [String:AnyObject]
                            let imgURL = imageProperties["image_src"]! as! String
                            //make mark without image
                            idNum += 1
                            let mark = MarkerDetails(name: "id\(idNum)", location:(CLLocationCoordinate2D(latitude: markerLat!, longitude: markerLong!)), imageSRC: imgURL)
                            self.markerDetailsArray.append(mark)
                            
                        }
                    }
                }
            }
        }
    }


//GETS MARKERS OUT OF ARRAY(No images)
private func getMarkers(){
    for Detail in self.markerDetailsArray{
        self.clusterManager?.addItem(Detail)
        
    }
}


//PLACE THE MARKER ON THE MAP
private func makeMarker(location: CLLocationCoordinate2D, title: String, imageSRC:String){
    //let img =  UIImage.init(view: myView)
    DispatchQueue.main.async(execute: {
        self.networkController.getImage(url: imageSRC, completion: { image in
            let marker = GMSMarker()
            //marker.icon = image//self.cusMarker(image: image)
            marker.position = location
            marker.title = title
            //self.clusterManager?.addItem(marker)
            //marker.map = self.mapView
            
        })
    })
    
}

/*--------------------------------------------------------------------- END MAP FUNCTIONS ------------------------------------------------------------------------------------*/
}

//CLLocationManagerDelegate
extension MapViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            userLat = location.coordinate.latitude
            userLong = location.coordinate.longitude
            print(userLat!)
            print(userLong!)
            LocationManager.stopUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse || status == .authorizedAlways {
            self.mapView?.isMyLocationEnabled = true
            self.mapView?.settings.myLocationButton = true
            LocationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error.localizedDescription)
    }
}

extension UIImage{
    convenience init(view: UIView) {
        
        UIGraphicsBeginImageContextWithOptions(view.bounds.size, view.isOpaque, 0.0)
        view.drawHierarchy(in: view.bounds, afterScreenUpdates: false)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        self.init(cgImage: (image?.cgImage)!)
        
    }
}
