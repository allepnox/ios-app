//
//  PostingControllerViewController.swift
//  Social-Local
//
//  Created by Muhammad Nabil Hairul Fadilah on 12/04/2017.
//  Copyright © 2017 Epnox. All rights reserved.
//

import UIKit
import GooglePlaces
import GooglePlacePicker

class PostingController: UIViewController {

    @IBOutlet weak var imagePreview: UIImageView!
    @IBOutlet weak var coinCount: UILabel!
    @IBOutlet weak var caption: UITextField!
    @IBOutlet weak var location: UIButton!
    @IBOutlet weak var sendLocalSocial: UIButton!
    @IBOutlet weak var attribution: UITextView!
    
    var placesClient: GMSPlacesClient!
    var takenPhoto = UIImage()
    var cropedPhoto = UIImage()
    var GPS = [String:Double]()
    var locationName = String()
    var imageBase64 = String()
    let current_unix = NSDate().timeIntervalSince1970
    
    private let LocationManager = CLLocationManager()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.endEditing(true)
        
        placesClient = GMSPlacesClient.shared()
        LocationManager.requestAlwaysAuthorization()
        
        cropedPhoto = cropImage(imageToCrop: takenPhoto, toRect: CGRectMake(
            takenPhoto.size.height/4,
            0,
            takenPhoto.size.width,
            takenPhoto.size.width)
        )
        
        changetoBase64(image: takenPhoto)
        imagePreview.image = takenPhoto
        coinCount.text = profile.remaining_coin
        
        getCurrentLocation()
        
        // Do any additional setup after loading the view.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        //let barViewControllers = segue.destination as! UITabBarController
        //let nav = barViewControllers.viewControllers![0] as! UINavigationController

        if segue.identifier == "postedSegue"
        {
            let barViewControllers = segue.destination as! UITabBarController
            let destinationViewController = barViewControllers.viewControllers?[0] as! MapViewController
                destinationViewController.postedGPS = self.GPS
        }
        else if segue.identifier == "shopSegue"{
            if let shopController:ShopController = segue.destination as? ShopController{
                shopController.coin = coinCount.text
            }
        }else{
            return
        }
    }

    override func touchesBegan(_: Set<UITouch>, with: UIEvent?){
        self.caption.resignFirstResponder()
    }
    
    func getCurrentLocation (){
        placesClient.currentPlace(callback: { (placeLikelihoodList, error) -> Void in
            if let error = error {
                print("Pick Place error: \(error.localizedDescription)")
                return
            }
            
            self.location.setTitle("No current place", for: UIControlState())
            
            if let placeLikelihoodList = placeLikelihoodList {
                let place = placeLikelihoodList.likelihoods.first?.place
                if let place = place {
                    self.locationName = place.name
                    self.location.setTitle(place.name,for:UIControlState())
                    self.GPS = [
                        "lat" : place.coordinate.latitude,
                        "long": place.coordinate.longitude
                    ]
                }
            }
        })
    }
    
    func changetoBase64(image:UIImage){
        
        //Now use image to create into NSData format
        let imageData = UIImageJPEGRepresentation(image, 0.1)
        
        self.imageBase64 = imageData!.base64EncodedString()
    }
    
    func cropImage(imageToCrop:UIImage, toRect rect:CGRect) -> UIImage{
        
        let imageRef:CGImage = imageToCrop.cgImage!.cropping(to: rect)!
        let image = UIImage(cgImage: imageRef, scale: imageToCrop.scale, orientation: imageToCrop.imageOrientation)
        return image
    }
    
    func CGRectMake(_ x: CGFloat, _ y: CGFloat, _ width: CGFloat, _ height: CGFloat) -> CGRect {
        return CGRect(x: x, y: y, width: width, height: height)
    }

    
    @IBAction func postButton(_ sender: Any) {
        let post: [String:AnyObject] = [
            "userid"        : user.userID! as AnyObject,
            "location"      : self.locationName as AnyObject,
            "lat"           : self.GPS["lat"] as AnyObject,
            "long"          : self.GPS["long"] as AnyObject,
            "caption"       : self.caption.text as AnyObject,
            "hours_expired" : 24 as AnyObject,
            "create_timestamp" : self.current_unix as AnyObject,
            "image"         : self.imageBase64 as AnyObject,
            "session_key"   : user.sessionKey! as AnyObject,
        ]
        
        let headers = [String:String]()
        APIController().postRequest(purpose: "insert_image", parameter: post, headers: headers){result in
            
            let alert = UIAlertController(title: "Success", message: "Profile Updated", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: {
                (alert: UIAlertAction!) in
                        self.performSegue(withIdentifier: "postedSegue", sender: self)
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func backButton(_ sender: Any) {
        if let nav = self.navigationController {
            nav.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func buyCoinButton(_ sender: Any) {
        performSegue(withIdentifier: "shopSegue", sender: nil)
    }
    @IBAction func pickPlace(_ sender: UIButton) {
        let config = GMSPlacePickerConfig(viewport: nil)
        let placePicker = GMSPlacePicker(config: config)
        
        placePicker.pickPlace(callback: { (place, error) -> Void in
            if let error = error {
                print("Pick Place error: \(error.localizedDescription)")
                return
            }
            
            guard let place = place else {
                print("No place selected")
                return
            }
            
            self.locationName = place.name
            self.location.setTitle(place.name,for:UIControlState())
            self.GPS = [
                "lat" : place.coordinate.latitude,
                "long": place.coordinate.longitude
            ]
        })
    }
}
