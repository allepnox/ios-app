//
//  first_containerViewController.swift
//  Social-Local
//
//  Created by Muhammad Nabil Hairul Fadilah on 28/03/2017.
//  Copyright © 2017 Epnox. All rights reserved.
//

import UIKit

class ChatController: UIViewController,UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableChat: UITableView!
    var arrayList = [AnyObject]()
    var count = 0
    
    var messages = ["hye","hello","wasap","yoo","dude","kiddo"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        APIController().getRequest(purpose: "get_user_follower") { data in
            self.arrayList = data["results"] as! [AnyObject]
            self.count = data["follower_count"] as! Int;()
            self.tableChat.reloadData()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableChat.dequeueReusableCell(withIdentifier: "chatCell", for: indexPath) as! ProfileCell
        
        cell.photoChat.layer.cornerRadius = cell.photoChat.frame.size.width / 2
        cell.photoChat.clipsToBounds = true
        
        let item = self.arrayList[indexPath.row]
        
        DispatchQueue.main.async(execute: {
            if !(item["image_src"] is NSNull){
                let urlImage = item["image_src"] as! String
                cell.photoChat.sd_setImage(with: URL(string: urlImage))
            }
        })
        cell.nameChat.text = item["username"] as! String?
        cell.messageChat.text = self.messages[indexPath.row]
        
        
        return cell
    }
}
