//
//  ViewController.swift
//  Social-Local
//
//  Created by Epnox on 21/03/2017.
//  Copyright © 2017 Epnox. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //use if to check if login flag is true
        //if true send to main page
        //if flase send to login
        self.performSegue(withIdentifier: "loginView", sender: self)
    }
}

