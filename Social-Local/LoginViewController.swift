//
//  LoginViewController.swift
//  Social-Local
//
//  Created by Remigius Kalimba on 22/03/2017.
//  Copyright © 2017 Epnox. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth

class LoginViewController: UIViewController {

    //VARIABLES
    @IBOutlet private weak var userEmailTextField: UITextField!
    @IBOutlet private weak var userPasswordTextField: UITextField!
    var passwordGlobal: String?
    var emailGlobal : String?
    var loginDict: [String: String] = [:]
    var loginFlag = false
    
    //Instances
    let networkController = APIController()
    let Cache = CacheController()
    
    
    //FUCNTIONS
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "loginToTabController"{
            
            APIController().getRequest(purpose: "get_user_profile"){data in
                
                var image_src = String()
                
                if !(data["image_src"] is NSNull){
                    image_src = data["image_src"] as! String
                }
                
                let _ = profile(
                    remaining_coin: data["remaining_coin"] as! String,
                    phone         : data["phone_no"]       as! String,
                    follower_count: data["follower_count"] as! Int,
                    image_src     : image_src,
                    gender        : data["gender"]         as! String,
                    date_of_birth : data["date_of_birth"]  as! Double,
                    post_count    : data["post_count"]     as! Int,
                    username      : data["username"]       as! String,
                    hometown      : data["hometown"]       as! String
                )
                
                var storeValue : [String:AnyObject] = [:]
                
                for (key,value) in data{
                    if value is NSNull {
                        storeValue[key] = "empty" as AnyObject
                    }else{
                        storeValue[key] = value
                    }
                }
                
                self.Cache.setCache(value: storeValue, key: "Profile")
                self.Cache.sync()
            }

        }
    }

    //Alerts
    private func alertShow(title: String, messsage: String){
        let alert = UIAlertController(title: title, message: messsage, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: {
            (action) in alert.dismiss(animated: true, completion: nil)
        }))
        DispatchQueue.main.async {
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    //CHECK EMAIL VALIDITY
    private func checkValidity(email: String, password: String){
        if (email.contains("@")){
            let _ = user.init(email: email,password: password)
            self.loginToAPP()
        }else{
            alertShow(title: "Error", messsage: "Not a valid email")
            return
        }
    }
    
    //FIREBASE LOGIN
        func fire_Login(email: String, password: String){
        FIRAuth.auth()?.signIn(withEmail: email, password: password) { (user, error) in
            if let fire_Session_Key = FIRAuth.auth()?.currentUser?.uid {
                //set globals
                let _ = firebase.init(fire_Session_Key: fire_Session_Key)
                //Cache
                UserDefaults.standard.setValue(fire_Session_Key, forKey: "fire_session_key")
                UserDefaults.standard.synchronize()
            } else {
                print("FireBase No such user")
            }
        }
    }
    
    //APP LOGIN
    func loginToAPP(){
        networkController.getRequest(purpose: "user_login"){data in
            print(data)
            if !data.isEmpty{
                if data["status"] as! Int == 1{
                    print(data["status"]!)
                    let _ = user.init(key: data["session_key"]! as? String, id: data["user_id"]! as? String)
                    
                    //Add to cache
                    self.loginDict = [
                        "sessionKey"    :data["session_key"] as! String,
                        "userID"        :data["user_id"] as! String,
                        "email"         : user.email!,
                        "password"      : user.password!
                        
                    ]
                    self.Cache.setCache(value: self.loginDict, key: "User")
                    self.Cache.sync()
                    self.loginFlag = true
                    
                    self.doSegue()
                }else{
                    
                    self.alertShow(title: "Error", messsage: "Incorrect Email/Password combination")
                    self.loginFlag = false
                }
            }else{
                print("unable return value", "probably internet not working :)")
                self.alertShow(title: "Error", messsage: "Check Internet Connecetion")
                print(data)
                self.loginFlag = false
            }
        }
    }
    
    
    private func doSegue(){
        print(loginFlag)
            DispatchQueue.main.async {
                self.fire_Login(email:user.email!, password: user.password!)
                self.performSegue(withIdentifier: "loginToTabController", sender: self)
        }
    }


    //WHEN LOGIN TAPPED
    @IBAction private func loginButtonTapped(_ sender: UIButton) {
        guard let email:String = userEmailTextField.text , !email.isEmpty else {
            //highlight textbox red
            alertShow(title: "Error", messsage: "Invalid email")
            return
        }
        
        guard let password:String = userPasswordTextField.text , !password.isEmpty else {
            //highlight textbox red
            alertShow(title: "Error", messsage: "Invalid password")
            return
        }
        
        //CHECK IF DETAILS VALID
        self.checkValidity(email: email, password: password)
        
    }
    @IBAction func registerButtonTapped(_ sender: UIButton) {
        print("register")
    }
}







