//
//  RegisterSplashController.swift
//  Social-Local
//
//  Created by Epnox on 08/05/2017.
//  Copyright © 2017 Epnox. All rights reserved.
//

import UIKit
import FirebaseAuth

class RegisterSplashController: UIViewController {
    //VARIABLES
    private var loginDict = [String: String]()
    
    //INSTANCES
    let NetworkController = APIController()
    let Cache = CacheController()
    let LoginControler = LoginViewController()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("View did load")

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        print("View did appear")
        self.loginToAPP()
    }
    
    //APP LOGIN
    func loginToAPP(){
        NetworkController.getRequest(purpose: "user_login"){data in
            print(data)
            if !data.isEmpty{
                if data["status"] as! Int == 1{
                    print(data["status"]!)
                    let _ = user.init(key: data["session_key"]! as? String, id: data["user_id"]! as? String)
                    //self.LoginControler.fire_Login(email: user.email!, password: user.password!)
                    
                    //Add to cache
                    self.loginDict = [
                        "sessionKey"    :data["session_key"] as! String,
                        "userID"        :data["user_id"] as! String,
                        "email"         : user.email!,
                        "password"      : user.password!
                        
                    ]
                    self.Cache.setCache(value: self.loginDict, key: "User")
                    self.Cache.sync()
                    
                    DispatchQueue.main.async {
                        self.performSegue(withIdentifier: "regSplashToNav", sender: self)
                    }
                }else{
                 print("Incorrect email and password combo")
                }
            }else{
                print("unable return value", "probably internet not working :)")
            }
        }
    }

    

}
 
