//
//  SphericalMercatorProjection.swift
//  google-maps-ios-utils-swift
//
//  Created by Eyal Darshan on 24/05/2016.
//  Copyright © 2016 eyaldar. All rights reserved.
//

import CoreLocation

class SphericalMercatorProjection {
    fileprivate let _worldWidth: Double
    
    init(worldWidth: Double) {
        _worldWidth = worldWidth
    }
    
    convenience init() {
        self.init(worldWidth: 1)
    }
    
    func pointToCoordinate(_ point: GQTPoint) -> CLLocationCoordinate2D {
        let x = point.x / _worldWidth - 0.5
        let lng = x * 360
        
        let y = 0.5 - (point.y / _worldWidth)
        let lat = 90 - ((atan(exp(-y * 2 * Double.pi)) * 2) * (180.0 / Double.pi))
        
        return CLLocationCoordinate2D(latitude: lat, longitude: lng)
    }
    
    func coordinateToPoint(_ coordinate: CLLocationCoordinate2D) -> GQTPoint {
        let x = coordinate.longitude / 360 + 0.5
        let siny = sin(coordinate.latitude / 180.0 * Double.pi)
        let y = 0.5 * log((1 + siny) / (1 - siny)) / -(2 * Double.pi) + 0.5
        
        return GQTPoint(x: x * _worldWidth, y: y * _worldWidth)
    }
}
