//
//  GDefaultClusterMarkerIconView.swift
//  google-maps-ios-utils-swift
//
//  Created by Eyal Darshan on 24/05/2016.
//  Copyright © 2016 eyaldar. All rights reserved.
//

import UIKit

final class GDefaultClusterMarkerIconView: UIView {
    
    static let TBScaleFactorAlpha:Float = 0.3
    static let TBScaleFactorBeta:Float = 0.4
    
    fileprivate var count:Int?
    fileprivate var countLabel:UILabel?
    
    init(count: Int) {
        let size = CGFloat(roundf(GDefaultClusterMarkerIconView.TBScaledValueForValue(Float(count), multiplier: 44.0)))
        super.init(frame: CGRect(origin: CGPoint.zero, size: CGSize(width: size, height: size)))
        
        initialize(count)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        initialize(1)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initialize(1)
    }
    
    func initialize(_ count: Int) {
        self.backgroundColor = UIColor.clear
        self.clipsToBounds = true
        
        setupLabel()
        setCount(count)
    }
    
    func update(_ count: Int) {
        self.backgroundColor = UIColor.clear
        setCount(count)
    }
    
    func TBCenterRect(_ rect:CGRect, center: CGPoint) -> CGRect {
        let r = CGRect(x: center.x - rect.size.width/2.0, y: center.y - rect.size.height/2.0, width: rect.size.width, height: rect.size.height)
        return r
    }
    
    func TBRectCenter(_ rect: CGRect) -> CGPoint {
        return CGPoint(x: rect.midX, y: rect.midY);
    }
    
    func setupLabel() {
        countLabel = UILabel(frame: self.frame)
        countLabel?.backgroundColor = UIColor.clear
        countLabel?.textColor = UIColor.white
        countLabel?.textAlignment = NSTextAlignment.center
        countLabel?.shadowColor = UIColor(white: 0, alpha: 0.75)
        countLabel?.shadowOffset = CGSize(width: 0, height: -1)
        countLabel?.adjustsFontSizeToFitWidth = true
        countLabel?.numberOfLines = 1
        countLabel?.font = UIFont.boldSystemFont(ofSize: 12)
        countLabel?.baselineAdjustment = UIBaselineAdjustment.alignCenters
        
        addSubview(countLabel!)
    }
    
    func setCount(_ count:Int) {
        self.count = count
        let size = CGFloat(roundf(GDefaultClusterMarkerIconView.TBScaledValueForValue(Float(count), multiplier: 44.0)))
        
        let newBounds = CGRect(x: 0, y: 0, width: size, height: size)
        frame = TBCenterRect(newBounds, center: self.center)
        
        let newLabelBounds:CGRect = CGRect(x: 0, y: 0, width: newBounds.size.width, height: newBounds.size.height)
        countLabel?.frame = TBCenterRect(newLabelBounds, center: TBRectCenter(newBounds))
        countLabel?.text = String(count)
        
        self.setNeedsDisplay()
    }
    
    override func draw(_ rect: CGRect) {
        let context = UIGraphicsGetCurrentContext()
        
        context?.setAllowsAntialiasing(true)
        
        let outerCircleStrokeColor = UIColor(white: 0, alpha: 0.25)
        let innerCircleStrokeColor = UIColor.white
        var innerCircleFillColor = UIColor(red: (255.0 / 255.0), green: (95.0 / 255.0), blue: (42.0 / 255.0), alpha: 1.0)
        if(count == 1) {
            innerCircleFillColor = UIColor(red: (95.0 / 255.0), green: (150.0 / 255.0), blue: (42.0 / 255.0), alpha: 1.0)
        }
        
        let circleFrame = rect.insetBy(dx: 4, dy: 4)
        
        outerCircleStrokeColor.setStroke()
        context?.setLineWidth(5.0)
        context?.strokeEllipse(in: circleFrame)
        
        innerCircleStrokeColor.setStroke()
        context?.setLineWidth(4.0)
        context?.strokeEllipse(in: circleFrame)
        
        innerCircleFillColor.setFill()
        context?.fillEllipse(in: circleFrame)
    }
    
    static func TBScaledValueForValue(_ value:Float, multiplier:Float) -> Float {
        // Multiplier * (1/e^(-Alpha * X^(Beta)))
        return multiplier * (1.0 / (1.0 + expf(-1 * TBScaleFactorAlpha * powf(value, TBScaleFactorBeta))))
    }
}
