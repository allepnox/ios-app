//
//  ViewController.swift
//  google-maps-ios-utils-swift
//
//  Created by Eyal Darshan on 24/05/2016.
//  Copyright © 2016 eyaldar. All rights reserved.
//

import GoogleMaps
import UIKit

class ViewController2: UIViewController {
    let kStartingLocation = CLLocationCoordinate2D(latitude: 51.503186, longitude: -0.126446)
    let kStartingZoom: Float = 9.5
    
    fileprivate var _mapView: GMSMapView!
    fileprivate var _clusterManager: GClusterManger!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let cameraPosition = GMSCameraPosition.camera(withTarget: kStartingLocation, zoom: kStartingZoom)
        
        _mapView = GMSMapView(frame: CGRect.zero)
        _mapView.camera = cameraPosition
        _mapView.isMyLocationEnabled = true
        _mapView.settings.myLocationButton = true
        _mapView.settings.compassButton = true
        
        self.view = _mapView
        
        _clusterManager = GClusterManger(mapView: _mapView,
                                         algorithm: NonHierarchicalDistanceBasedAlgorithm(),
                                         renderer: GDefaultClusterRenderer(mapView: _mapView))
        _clusterManager.isZoomDependent = true
        
        _mapView.delegate = _clusterManager
        _clusterManager.delegate = self as? GMSMapViewDelegate
        
        for id in 0...20000 {
            let spot = generateSpot(id)
            _clusterManager.addItem(spot)
        }
    }
    
    fileprivate func generateSpot(_ id: Int) -> Spot {
        let position = CLLocationCoordinate2D(latitude: getRandomNumberBetween(51.38494009999999, max:51.6723432),
                                                 longitude: getRandomNumberBetween(-0.3514683, max: 0.148271))
        
        let spot = Spot(id: "\(id)", position: position)
        
        return spot
    }
    
    fileprivate func getRandomNumberBetween(_ min: Double, max: Double) -> Double {
        return min + (max - min) * drand48()
    }
}

extension ViewController: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        print("Marker Tapped!!")
        return true
    }
}
