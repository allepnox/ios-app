//
//  GClusterAlgorithm.swift
//  google-maps-ios-utils-swift
//
//  Created by Eyal Darshan on 24/05/2016.
//  Copyright © 2016 eyaldar. All rights reserved.
//

import Foundation
import GoogleMaps
import UIKit

protocol GClusterAlgorithm {
    func addItem(_ item: GClusterItem)
    func removeItems()
    func removeItemsNotInRectangle(_ bounds: GQTBounds)
    func hideItemsNotInBounds(_ bounds: GMSCoordinateBounds)
    
    func removeItem(_ item: GClusterItem)
    func removeClusterItemsInSet(_ set: NSSet)
    func containsItem(_ item: GClusterItem) -> Bool
    func getClusters(_ zoom: Double) -> NSSet
}
